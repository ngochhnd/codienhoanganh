<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->integer('total_money');
            $table->integer('guest_pay')->default(0);
            $table->tinyInteger('status')->default(2)->comment('2: hoat dong; 1 trang thai tam huy don');
            $table->tinyInteger('type')->comment('1: HĐ bán lẻ, 2: HĐ bán buôn');
            $table->string('note')->nullable();
            $table->bigInteger('user_delete_id')->nullable();
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });

        Schema::table('invoices', function($table) {
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('invoices');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
