<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'full_name' => 'Hoàng Văn Ánh',
                'name' => 'hoanganh',
                'phone' => '1234567890',
            ],
            [
                'full_name' => 'Nguyễn Văn Chuân',
                'name' => 'nguyenchuan',
                'phone' => '1234567891',
            ],
            [
                'full_name' => 'Hoàng Văn Ngọc',
                'name' => 'ngochv',
                'phone' => '0963140067',
            ],
        ];

        foreach ($datas as $iteam) {
            $user = User::where('full_name', $iteam['full_name'])
                ->where('name', $iteam['name'])->first();
            if (!$user) {
                $user = User::create([
                    'full_name' => $iteam['full_name'],
                    'name' => $iteam['name'],
                    'phone' => $iteam['phone'],
                    'password' => bcrypt('123456789'),
                ]);
                $user->assignRole('sup admin');
            }
        }
    }
}
