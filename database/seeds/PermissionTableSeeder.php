<?php

use App\DataPermission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            // thêm role admin
            $role = Role::where('name', 'sup admin')->first();
            if (!$role) {
                Role::create(['name' => 'sup admin']);
                Log::info('Thêm chức vụ admin thành công!');
            }

            $dataPermission = new DataPermission;
            $datas = $dataPermission->getData();
            $tblPer = Permission::all();

            $arrPer = [];
            foreach ($datas as $key => $iteam) {
                $per = Permission::where('name', $key)->first();
                if ($per) {
                    Log::error('Permission key ' . $key  . ' đã tồn tại!');
                } else {
                    Permission::create(['name' => $key]);
                    $arrPer[] = $key;
                }
            }
            Log::info('Cập nhật quyền thành công!');

            // set permision for roles 'sup admin'
            if (count($arrPer)) {
                $roleSupAdmin = Role::where('name', 'sup admin')->first();
                $roleSupAdmin->syncPermissions($arrPer);
                Log::info('Cập nhật sup admin thêm các quyền: ' . implode(", ", $arrPer) . ' thành công!');
            }
        } catch (Exception $ex) {
            Log::info($ex);
        }
    }
}
