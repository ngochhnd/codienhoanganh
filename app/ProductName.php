<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductName extends Model
{
    protected $fillable = [
        'name', 'status'
    ];

     /**
     * tìm kiếm tên sản phẩm trong hóa đơn.
     */
    public function searchNameProduction($name)
    {
        return self::where('name', 'LIKE', '%' . $name . '%')
            ->get(['id', 'name as text']);
    }
}
