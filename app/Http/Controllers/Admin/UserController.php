<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Exception;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * get list employee.
     *
     * @return
     */
    public function index()
    {
        if(!Auth::user()->hasPermissionTo('create user')) {
            return view('admin.error_permission');
        }
        $user = new User;
        $param = [
            'users' => $user->getAll(),
        ];
        return view('admin.users.index', $param);
    }
    
    /**
     * get list employee.
     *
     * @return
     */
    public function create()
    {
        if(!Auth::user()->hasPermissionTo('create user')) {
            return view('admin.error_permission');
        }
        return view('admin.users.create');
    }

    /**
     * edit employee
     * @param  [int] $id
     * @return [type]
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermissionTo('create user')) {
            return view('admin.error_permission');
        }
        $user = User::withTrashed()->find($id);
        if (!$user) {
            return redirect()->back()->withErrors('Không tìm thấy nhân viên');
        }
        $param = [
            'user' => $user,
        ];
        return view('admin.users.edit', $param);
    }

    /**
     * update employee
     * @param  [int] $id
     * @return [type]
     */
    public function update($id, Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create user')) {
            return view('admin.error_permission');
        }
        $user = User::withTrashed()->find($id);
        if (!$user) {
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
        $validator = Validator::make($request->all(), [
                'full_name' => ['required', 'string', 'max:255'],
                'name' => ['required', 'string', 'max:255', 'unique:users,name,' . $id],
            ]
        );
        if ($request->status == User::ACTIVE) {
            $deletedAt = null;
        } else {
            $deletedAt = Carbon::now();
        }
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $data = $request->all();
        try {
            $user->update([
                'name' => $data['name'],
                'full_name' => $data['full_name'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'address' => $data['address'],
                'birthday' => $data['birthday'],
                'deleted_at' => $deletedAt,
            ]);
            return redirect()->back()->with('message', 'Cập nhật thành công!');
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
    }

    /**
     * delete 
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        if(!Auth::user()->hasPermissionTo('create user')) {
            return view('admin.error_permission');
        }
        $user = User::find($id);
        if ($user->hasRole('sup admin')) {
            return redirect()->back()->withErrors('Nhân viên này không thể xóa');
        }
        if (!$user) {
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
        try {
            $user->delete();
            return redirect()->back()->with('message', 'Xóa thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
    }

    public function changePassWord()
    {
       return view('admin.users.change_passowrd');
    }

    public function saveChangePassWord(Request $request)
    {
        $messages = [
            'passwordOld.required' => 'Không để trống mật khẩu cũ',
            'passwordNew.required' => 'Không để trống mật khẩu mới',
            'passwordAgain.required' => 'Không để trống mật khẩu nhập lại',
            'passwordNew.min' => 'Mật khẩu phải lớn hơn 8 ký tự',
            'passwordAgain.same' => 'Mật khẩu nhập lại không giống mật khẩu mới',
        ];

        $this->validate($request, [
            'passwordOld' => 'required',
            'passwordNew' => 'required|min:8|string',
            'passwordAgain' => 'required|same:passwordNew',
        ], $messages);


        $data = $request->all();

        $user = Auth::user();
        if(!\Hash::check($data['passwordOld'], $user->password)) {
            return redirect()->back()->withErrors('Mật khẩu hiện tại không đúng!');
        }else{
            $user->password = bcrypt($request->get('passwordNew'));
            $user->save();
        }
        return redirect()->back()->with("message","Thay đổi mật khẩu thành công!");
    }
}
