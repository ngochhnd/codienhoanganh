<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index()
    {
        if(!Auth::user()->hasPermissionTo('create permission role')) {
            return view('admin.error_permission');
        }
        $arrPres = $this->getAllPermissions();
        $arrRoles = $this->getAllRoles();
        $arrPreRoles = [];
        $arrPreRoleName = [];
        foreach ($arrRoles as $role) {
            $arrPreRoles[$role->id] = $role->permissions()->select("id", "name")->get()->toArray();
        }
        foreach ($arrPreRoles as $key => $pers) {
            foreach ($pers as $item) {
                $arrPreRoleName[$key][] = $item['name'];
            }
        }
        $param = [
            'arrPres' => $arrPres,
            'arrRoles' => $arrRoles,
            'arrPreRoleName' => $arrPreRoleName,
            'reponPreRoleName' => response($arrPreRoleName),
        ];

        return view('admin.permissions.index', $param);
    }

    /**
     * get all permission
     * @return [type]
     */
    public function getAllPermissions()
    {
        return Permission::select("id", "name")->get();
    }

    /**
     * get all permission
     * @return [type]
     */
    public function getAllRoles()
    {
        return Role::select("id", "name")
            ->where('name', '<>', 'sup admin')
            ->get();
    }

    /**
     * update permission of roles
     * @return [type] [description]
     */
    public function update(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create role user')) {
            return view('admin.error_permission');
        }
        $validatedData = $request->validate([
                'role_id' => 'required'
            ], [
                'role_id.required' => 'Chức vụ không để trống.',
            ]);
        $role =  Role::find($request->role_id);
        $arrPermission = explode(",",$request->permission);
        if (!$role) {
            return redirect()->back()->withErrors('Chức vụ không tồn tại');
        }
        if ($role->name === 'sup admin') {
            return redirect()->back()->withErrors('Không cấp quyền cho chức vụ này.');   
        }
        $rolesOld = $role->getAllPermissions();
        $pers = Permission::whereIn('name', $arrPermission)->get();

        try {
            if (count($rolesOld)) {
                foreach ($rolesOld as $item) {
                    $role->revokePermissionTo($item->name);
                }
            }
            if (count($pers)) {
                foreach ($pers as $item) {
                    $role->givePermissionTo($item->name);
                }
            }
            return redirect()->back()->with('message', 'Cấp chức vụ cho quyền thành công!');
        } catch (Exception $e) {
            report($e);
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
    }
}
