<?php

namespace App\Http\Controllers\Admin;

use App\Invoice;
use App\InvoicePay;
use App\Customer;
use App\CustomerInvoicePay;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    const LIMIT = 50;

    /**
     * [index description]
     * @param  Request  $request
     * @param  Customer $customer
     * @return [type]
     */
    public function index(Request $request, Customer $customer)
    {
        if(!Auth::user()->hasPermissionTo('create customer')) {
            return view('admin.error_permission');
        }

        $dataSearch = [
            'customer_name' => $request->get('customer_name'),
            'customer_phone' => $request->get('customer_phone'),
        ];
        $params = [
            'customers' => $customer->getAll($dataSearch),
            'dataSearch' => $dataSearch,
        ];
        return view('admin.customers.index', $params); 
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        if(!Auth::user()->hasPermissionTo('create customer')) {
            return view('admin.error_permission');
        }
        return view('admin.customers.create'); 
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        if(!Auth::user()->hasPermissionTo('create customer')) {
            return view('admin.error_permission');
        }
        $customer = Customer::find($id);
        if (!$customer) {
            return redirect()->back()->withErrors('Không tìm khách hàng!');
        }
        $params = [
            'customer' => $customer
        ];
        return view('admin.customers.edit', $params); 
    }

    /**
     * [index description]
     * @param  Request  $request
     * @param  Customer $customer
     * @return [type]
     */
    public function store(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create customer')) {
            return view('admin.error_permission');
        }
        if ($request->type == Customer::WHOLESALE) {
            $data = [
                'name' => ['required', 'string', 'max:255', 'unique:customers'],
                'phone' => ['required', 'unique:customers'],
            ];
            $message = [
                'name.required' => 'Tên khách hàng không để trống',
                'name.unique' => 'Tên khách hàng bán sỉ đã tồn tại',
                'phone.required' => 'Số điện thoại không để trống',
                'phone.unique' => 'Số điện thoại đã tồn tại'
            ];
        } else {
            $data = [
                'phone' => ['required', 'unique:customers,phone,'],
            ];
            $message = [
                'phone.required' => 'Số điện thoại không để trống',
                'phone.unique' => 'Số điện thoại bán lẻ đã tồn tại'
            ];
        }
        $validator = Validator::make($request->all(), $data, $message);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        try {
            Customer::create($request->all());
            return redirect()->back()->with('message', 'Thêm khách hàng thành công!');
        } catch (Exception $ex) {
            Log::info($ex);
            return redirect()->back()->withErrors('Có lỗi sảy ra!');
        }
    }

    /**
     * [index description]
     * @param  Request  $request
     * @param  Customer $customer
     * @return [type]
     */
    public function update($id, Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create customer')) {
            return view('admin.error_permission');
        }
        $customer = Customer::find($id);
        if (!$customer) {
            return redirect()->back()->withErrors('Không tìm khách hàng!');
        }
        if ($customer->type == Customer::WHOLESALE) {
            $data = [
                'name' => ['required', 'string', 'max:255', 'unique:customers,name,' . $id],
                'phone' => ['required', 'unique:customers,phone,' . $id],
            ];
            $message = [
                'name.required' => 'Tên khách hàng không để trống',
                'name.unique' => 'Tên khách hàng bán sỉ đã tồn tại',
                'phone.required' => 'Số điện thoại không để trống',
                'phone.unique' => 'Số điện thoại đã tồn tại'
            ];
        } else {
            $data = [
                'phone' => ['required', 'unique:customers,phone,' . $id],
            ];
            $message = [
                'phone.required' => 'Số điện thoại không để trống',
                'phone.unique' => 'Số điện thoại đã tồn tại'
            ];
        }
        $validator = Validator::make($request->all(), $data, $message);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        try {
            $customer->update($request->all());
            return redirect()->back()->with('message', 'Cập nhật thành công!');
        } catch (Exception $ex) {
            Log::info($ex);
            return redirect()->back()->withErrors('Có lỗi sảy ra!');
        }
    }

    /**
     * [delete description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        if(!Auth::user()->hasPermissionTo('create customer')) {
            return view('admin.error_permission');
        }
        $customer = Customer::find($id);
        if (!$customer) {
            return redirect()->back()->withErrors('Không tìm khách hàng!');
        }
        $invoice = Invoice::where('customer_id', $id)->first();
        if ($invoice) {
            return redirect()->back()->withErrors('Khách hàng có hóa đơn không thể xóa!');
        }
        try {
            Customer::destroy($id);
            return redirect()->back()->with('message', 'Xóa khách hàng thành công!');
        } catch (Exception $ex) {
            Log::info($ex);
            return redirect()->back()->withErrors('Có lỗi sảy ra!');
        }
    }

    /**
     * tìm kiếm khách hàng
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getCustomerInvoice(Request $request)
    {
        $customer = new Customer;
        if ($request->input('type') != 'false') {
            $customers = $customer->searchCusWholesale($request->input('search', ''));
        } else {
            // $customers = $customer->searchCusRetails($request->input('search', ''), $request->input('phone', ''));
            $customers = [];
        }
        return ['results' => $customers];
    }

    /**
     * get thông tin khách hàng khi tạo hóa đơn
     * @param  Request $request
     * @return [type]
     */
    public function getCustomerInvoiceInfor(Request $request)
    {
        $params = [
            'total_debt' => 0,
            'id' => '',
            'name' => '',
            'phone' => '',
            'address' => '',
        ];
        $id = $request->valueName;
        $type = $request->type != 'false' ? Customer::WHOLESALE : Customer::RETAIL;
        $phone = $request->phone;

        $objCustomer = new Customer;
        if (is_numeric($id)) {
            $customer = $objCustomer->getCusInvoiceById($id);
            if (!$customer) {
                $customer = Customer::find($id);
                $params['phone'] = $customer->phone;
                $params['name'] = $customer->name;
                $params['id'] = $customer->id;
                $params['address'] = $customer->address;
                return $params;
            }
        } elseif (!empty($phone)) {
            $customer = $objCustomer->getCusInvoiceByPhone($type, $phone);
        } else {
            $customer = false;
        }
        if ($customer) {
            $params['total_debt'] += $customer->debt;
            $params['phone'] = $customer->phone;
            $params['name'] = $customer->name;
            $params['id'] = $customer->id;
            $params['address'] = $customer->address;
        }
        return $params;
    }

    /**
     * danh sách hóa đơn viên bán sỉ
     * @return [type] [description]
     */
    public function reportWholesale()
    {
        if(!Auth::user()->hasPermissionTo('report invoice')) {
            return view('admin.error_permission');
        }
        $customer = new Customer();
        $params = [
            'customers' => $customer->getCusInvoiceByType(Customer::WHOLESALE),
            'cateUser' => 'Danh sách khách hàng bán sỉ',
        ];
        return view('admin.customers.report_invoice', $params);
    }

    /**
     * danh sách hóa đơn viên bán lẻ
     * @return [type] [description]
     */
    public function reportRetail()
    {
        if(!Auth::user()->hasPermissionTo('report invoice')) {
            return view('admin.error_permission');
        }
        $customer = new Customer();
        $params = [
            'customers' => $customer->getCusInvoiceByType(Customer::RETAIL),
            'cateUser' => 'Danh sách khách hàng bán lẻ',
        ];
        return view('admin.customers.report_invoice', $params);
    }

    /**
     * get danh sách khách hàng nợ tiền
     * @return [type] [description]
     */
    public function getCustomerDebt()
    {
        $customer = new CustomerInvoicePay();

        $params = [
            'customers' => $customer->getCustomerDebt(),
        ];

        return view('admin.customers.report_customer_debt', $params);
    }

    /**
     * danh sách hóa đơn theo khách hàng
     * @return [type] [description]
     */
    public function getlistInvoice($id)
    {
        if(!Auth::user()->hasPermissionTo('report invoice')) {
            return view('admin.error_permission');
        }
        $customer = Customer::find($id);
        if (!$customer) {
            return redirect()->back()->withErrors('Không tìm khách hàng!');
        }
        $collection = Invoice::join('customers', 'customers.id', '=', 'invoices.customer_id')
            ->where('customers.id', $id)
            ->whereNull('invoices.deleted_at')
            ->orderBy('invoices.created_at', 'DESC')->paginate(static::LIMIT);
        $params = [
            'invoices' => $collection,
            'customer' => $customer,
        ];
        return view('admin.customers.list_invoice', $params);
    }

    public function historyPays($id)
    {
        $objInvoicePay = new InvoicePay();
        $cusPays = $objInvoicePay->getInvoicePayByCus($id);

        return view('admin.customers.history_pays', ['cusPays' => $cusPays]);
    }
}
