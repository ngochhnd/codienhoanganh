<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\CustomerInvoicePay;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\InvoiceDetail;
use App\InvoicePay;
use App\ProductName;
use App\User;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Log;

class InvoiceController extends Controller
{
    const LIMIT = 50;
    const MONEY_PENCE = 500;

    public function create()
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.error_permission');
        }
        $invoiceDetail = new InvoiceDetail;
        return view('admin.invoice.create_order', [
            'arrUnit' => $invoiceDetail->nameUnit(),
        ]);
    }

    /**
     * save invoice
     * @param  Request $request
     * @return [type]           [description]
     */
    public function save(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.error_permission');
        }
        $arrData = $this->getValueSerialize($request->data);
        $validator = Validator::make($arrData, [
            'customer_name' => ['required', 'max:255'],
            'name[]' => ['required'],
        ], [
            'customer_name.required' => 'Tên khách hàng không để trống',
            'customer_name.max' => 'Tên khách hàng lớn hơn 255 ký tự',
            'name[].required' => 'Đơn hàng không có sản phẩm',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        if ($arrData['type'] == Customer::WHOLESALE) {
            $customer = Customer::where('name', $arrData['customer_name'])->where('type', Customer::WHOLESALE)->first();
        } elseif (!empty($arrData['customer_phone'])) {
            $customer = Customer::where('name', $arrData['customer_name'])->where('type', Customer::WHOLESALE)->first();
        } else {
            $customer = false;
        }

        DB::beginTransaction();
        try {
            $dataDetail = $this->processingInvoiceDetail($arrData);
            $arrData['guests_pay'] = (int) str_replace('.', '', $arrData['guests_pay']);
            if (($dataDetail['total'] / self::MONEY_PENCE) > ($arrData['guests_pay'] / self::MONEY_PENCE)
                && ($dataDetail['total'] - $arrData['guests_pay']) < 999) {
                return response()->json(['error' => ['Làm tròn số tiền hóa đơn lên hoặc tiền nợ lớn hơn 1.000 đ']]);
            }
            if (($dataDetail['total'] / self::MONEY_PENCE) > ($arrData['guests_pay'] / self::MONEY_PENCE) &&
                empty($arrData['customer_phone'])) {
                return response()->json(['error' => ['Hóa đơn bán thiếu cần điền số điện thoại']]);
            }
            if (!$customer) {
                $customer_id = $this->processingCustemer($arrData, $customer);
            } else {
                $customer_id = $customer->id;
            }

            $dataInvoice = [
                'user_id' => Auth::id(),
                'customer_id' => $customer_id,
                'total_money' => $dataDetail['total'],
                'type' => $arrData['type'],
                'guest_pay' => $arrData['guests_pay'],
            ];

            $idInvoice = Invoice::create($dataInvoice)->id;

            //== get ids production ====
            $arrIdProd = [];
            foreach ($dataDetail['dataDetail'] as $key => $item) {
                $dataDetail['dataDetail'][$key]['invoice_id'] = $idInvoice;
                if (!is_numeric($dataDetail['dataDetail'][$key]['product_name_id'])) {
                    $idProd = ProductName::create(['name' => $dataDetail['dataDetail'][$key]['product_name_id']])->id;
                    $dataDetail['dataDetail'][$key]['product_name_id'] = $idProd;
                }
                InvoiceDetail::create($dataDetail['dataDetail'][$key]);
            }
            //== lay thong tin no cua user
            $customerPay = CustomerInvoicePay::where('customer_id', $customer_id)->first();
            if ($customerPay) {
                $customerPay->total_money_invoice += $dataInvoice['total_money'];
                $customerPay->total_guest_pay += $dataInvoice['guest_pay'];
                $customerPay->debt = $customerPay->total_guest_pay - $customerPay->total_money_invoice;
                if ($customerPay->debt < self::MONEY_PENCE) {
                    $customerPay->debt = 0;
                }
                $customerPay->update();
            } else {
                $data = [
                    'customer_id' => $customer_id,
                    'total_money_invoice' => $dataInvoice['total_money'],
                    'total_guest_pay' => $dataInvoice['guest_pay'],
                    'debt' => $dataInvoice['guest_pay'] - $dataInvoice['total_money'],
                ];

                if ($data['debt'] > 0 && $data['debt'] < self::MONEY_PENCE) {
                    $data['debt'] = 0;
                }

                CustomerInvoicePay::create($data);
            }

            $result = [
                'succes' => 1,
                'data' => $idInvoice,
            ];
            DB::commit();
            return response($result);
        } catch (Exception $ex) {
            DB::rollback();
            Log::info($ex);
            report($ex);
        }
    }

    /**
     * su ly du lieu post ajax serializeArray
     * @param  [array] $data
     * @return [type]
     */
    public function getValueSerialize($data)
    {
        $arrData = [];
        if (count($data)) {
            foreach ($data as $item) {
                if (strpos($item['name'], '[]') !== false) {
                    $arrData[$item['name']][] = $item['value'];
                } else {
                    $arrData[$item['name']] = $item['value'];
                }
            }
        }
        if (isset($arrData['type']) && $arrData['type'] == 'on') {
            $arrData['type'] = Customer::WHOLESALE;
        } else {
            $arrData['type'] = Customer::RETAIL;
        }
        if (isset($arrData['customer_name']) && is_numeric($arrData['customer_name'])) {
            $item = Customer::find($arrData['customer_name']);
            $arrData['customer_name'] = $item->name;
        }
        return $arrData;
    }

    /**
     * Lưu hóa đơn sử lý lưu thông tin khách hàng
     * @param  [array] $arrData
     * @return [int]
     */
    public function processingCustemer($arrData, $customer)
    {
        if ($customer) {
            return $customer_id = $customer->id;
        }

        $cus = new Customer;
        $customer = $cus->findCus($arrData['customer_name'], $arrData['customer_phone']);
            if ($customer) {
            $customer_id = $customer->id;
        } else {
            $data = [
                'name' => $arrData['customer_name'],
                'phone' => $arrData['customer_phone'],
                'type' => $arrData['type'],
                'address' => $arrData['customer_address'],
            ];
            $customer_id = Customer::create($data)->id;
        }
        return $customer_id;
    }

    /**
     * xử lý dữ liệu chi tiết hóa đơn
     * @param  [array] $arrData
     * @return [int]
     */
    public function processingInvoiceDetail($arrData)
    {
        $total = 0;
        $error = [];
        foreach ($arrData['name[]'] as $key => $value) {
            if (empty($value)) {
                continue;
            }
            if (empty($arrData['price[]'][$key]) || empty($arrData['quantity[]'][$key])) {
                $error = ['Đơn hàng không có số lượng hoặc đơn giá.'];
                break;
            }
            $dataDetail[$key] = [
                'unit_id' => $arrData['unit[]'][$key],
                'product_name_id' => $value,
                'price' => (int) str_replace('.', '', $arrData['price[]'][$key]),
                'quantity' => (int) $arrData['quantity[]'][$key],
                'discount' => empty($arrData['discount[]'][$key]) ? 0 : $arrData['discount[]'][$key],
            ];
            $totalTemp = Invoice::calculationDiscountInvoice($dataDetail[$key]['price'], $dataDetail[$key]['quantity'], $dataDetail[$key]['discount']);
            $total += $totalTemp;
        }
        return [
            'error' => $error,
            'dataDetail' => $dataDetail,
            'total' => (int) $total,
        ];
    }

    /**
     * in hoa don
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function printInvoice($id)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.invoice.print_order', [
                'invoices' => [],
                'arrUnit' => [],
            ]);
        }
        $invoice = new Invoice;
        $invoiceDetail = new InvoiceDetail;
        return view('admin.invoice.print_order', [
            'invoices' => $invoice->getAll($id),
            'arrUnit' => $invoiceDetail->nameUnit(),
        ]);
    }

    /**
     * in hoa don
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function printInvoiceA4($id)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.invoice.print_order', [
                'invoices' => [],
                'arrUnit' => [],
            ]);
        }
        $invoice = new Invoice;
        $invoiceDetail = new InvoiceDetail;
        return view('admin.invoice.print_orderA4', [
            'invoices' => $invoice->getAll($id),
            'arrUnit' => $invoiceDetail->nameUnit(),
        ]);
    }

    /**
     * in hoa don
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function printInvoiceBack($id)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.invoice.print_order', [
                'invoices' => [],
                'arrUnit' => [],
            ]);
        }
        $invoice = new Invoice;
        $invoiceDetail = new InvoiceDetail;
        return view('admin.invoice.print_order_back', [
            'invoices' => $invoice->getAll($id),
            'arrUnit' => $invoiceDetail->nameUnit(),
        ]);
    }

    /**
     * hiển thị all hóa đơn
     */
    public function index(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.error_permission');
        }
        $dataSearch = [
            'invoice_id' => $request->get('invoice_id'),
            'customer_name' => $request->get('customer_name'),
            'customer_date' => $request->get('customer_date'),
            'user_id' => $request->get('user_id'),
        ];
        $invoice = new Invoice();
        $collections = $invoice->getListAll($dataSearch);
        $user = new User();
        $param = [
            'invoices' => $collections,
            'dataSearch' => $dataSearch,
            'users' => $user->getAll(),
        ];
        return view('admin.invoice.index', $param);
    }

    /**
     * lưu trạng thái hủy đơn tạm thời
     * @param  Request $request
     * @return [type]
     */
    public function chaneStatusInvoice(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.error_permission');
        }

        $id = $request->id;
        $invoice = Invoice::find($id);
        if (!$invoice) {
            return redirect()->back()->withErrors('Không tìm thấy hóa đơn!');
        }
        if (!Auth::user()->hasPermissionTo('manager invoice')) {
            if ($invoice->user_id != Auth::id()) {
                return redirect()->back()->withErrors('Hóa đơn này không phải bạn tạo!');
            }
        }
        $note = array_key_exists('note', $request->all()) ? $request->note : $invoice->note;
        $data = [
            'status' => Invoice::DELETE,
            'note' => $note,
        ];
        $invoice->update($data);
        return redirect()->back()->with('message', 'Báo cáo hủy đơn thành công!');
    }

    /**
     * xóa hóa đơn với quyền quản lý hóa đơn
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        if(!Auth::user()->hasPermissionTo('manager invoice')) {
            return view('admin.error_permission');
        }
        $id = $request->id;
        $invoice = Invoice::find($id);
        if (!$invoice) {
            return redirect()->back()->withErrors('Không tìm thấy hóa đơn!');
        }
        $data = [
            'status' => $request->status,
            'user_delete_id' => Auth::id(),
        ];
        $invoice->update($data);
        return redirect()->back()->with('message', 'Hủy đơn thành công!');
    }

    /**
     * hiển thị các đơn cần duyệt hủy đơn
     * @return [type]
     */
    public function getInvoiceCensorship()
    {
        if(!Auth::user()->hasPermissionTo('manager invoice')) {
            return view('admin.error_permission');
        }
        $invoices = new Invoice;
        $param = [
            'invoices' => $invoices->getInvoiceCensorship(),
        ];
        return view('admin.invoice.mana_list', $param);
    }

    //============= report ===========
    /**
     * Báo cáo hóa đơn - theo start date - end date
     * @param  Request $request
     * @return [type]
     */
    public function reportInvoice(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('report invoice')) {
            return view('admin.error_permission');
        }
        $now = Carbon::now();
        $startDate = $request->get('startdate');
        $endDate = $request->get('enddate');
        if (empty($startDate)) {
            $startDate = $now;
        } else {
            $startDate = Carbon::parse($startDate);
        }
        if (empty($endDate)) {
            $endDate = $now;
        } else {
            $endDate = Carbon::parse($endDate);
        }
        $start = $startDate->format("Y-m-d");
        $end = $startDate->format("Y-m-d");

        $collection = Invoice::whereNull('invoices.deleted_at')
            ->whereDate('created_at', '>=', $startDate)
            ->whereDate('created_at', '<=', $endDate)
            ->orderBy('invoices.created_at', 'DESC')->paginate(static::LIMIT);
        $totalMoney = $collection->sum('guest_pay');
        $totalMoneyInvoice = $collection->sum('total_money');
        $param = [
            'invoices' => $collection,
            'totalMoneyInvoice' => $totalMoneyInvoice,
            'totalMoney' => $totalMoney,
            'date' => [
                'start' => $startDate->format("d-m-Y"),
                'end' => $endDate->format("d-m-Y"),
            ],
        ];
        return view('admin.invoice.report', $param);
    }

     /**
     * Báo cáo hóa đơn - theo start date - end date
     * @param  Request $request
     * @return [type]
     */
    public function reportInvoiceDebt(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('report invoice')) {
            return view('admin.error_permission');
        }
        
        $collection = Invoice::whereNull('invoices.deleted_at')
            ->where('total_money', '>', DB::raw('guest_pay'))
            ->orderBy('invoices.created_at', 'DESC')->paginate(static::LIMIT);

        $param = [
            'invoices' => $collection,
        ];
        return view('admin.invoice.report_debt', $param);
    }

    /**
     * tìm kiếm tên sản phẩm trong hóa đơn
     * @param  Request $request
     * @return [type]
     */
    public function searchNameProduction(Request $request)
    {
        $prodName = new ProductName();
        $prodNames = $prodName->searchNameProduction($request->input('term', ''));
        return ['results' => $prodNames];
    }

    /**
     * hiển thị danh sách hóa đơn theo id
     */
    public function list(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.error_permission');
        }
        $dataSearch = [
            'invoice_id' => $request->get('invoice_id'),
            'customer_name' => $request->get('customer_name'),
            'customer_date' => $request->get('customer_date'),
        ];

        $objInvoice = new Invoice();
        $collections = $objInvoice->getListAll($dataSearch, Auth::id());

        $param = [
            'invoices' => $collections,
            'dataSearch' => $dataSearch,
        ];
        return view('admin.invoice.list', $param);
    }

    /**
     * udpate duyệt không duyệt hóa đơn
     * @param  Request $request [description]
     * @return boolean          [description]
     */
    public function isCensorship(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('manager invoice')) {
            return view('admin.error_permission');
        }
        $objInvoice = Invoice::find($request->id);
        if (!$objInvoice) {
            return redirect()->route('admin.invoice.censorship')->withErrors('Không tìm thấy hóa đơn!');
        }
        $data = [
            'status' => $request->status,
        ];
        if (empty($objInvoice->deleted_at) && $request->status == Invoice::APPROVED) {
            $data['deleted_at'] = Carbon::now();
            $data['user_delete_id'] = Auth::id();
        } elseif (!empty($objInvoice->deleted_at) && $request->status == Invoice::DISAPPROVED) {
            $data['deleted_at'] = null;
        } else {

        }
        DB::beginTransaction();
        try {
            $customerPay = new CustomerInvoicePay();
            $cusPay = $customerPay->getCustomerPay($objInvoice->customer_id);
            $dataCusPay = [];
            if ($cusPay) {
                if (empty($objInvoice->deleted_at) && $request->status == Invoice::APPROVED) {
                    $dataCusPay = [
                        'total_money_invoice' => $cusPay->total_money_invoice - $objInvoice->total_money,
                        'total_guest_pay' => $cusPay->total_guest_pay - $objInvoice->guest_pay,
                    ];
                } elseif (!empty($objInvoice->deleted_at) && $request->status == Invoice::DISAPPROVED) {
                    $dataCusPay = [
                        'total_money_invoice' => $cusPay->total_money_invoice + $objInvoice->total_money,
                        'total_guest_pay' => $cusPay->total_guest_pay + $objInvoice->guest_pay,
                    ];
                } else {
                    $objInvoice->update($data);
                    DB::commit();
                    return redirect()->route('admin.invoice.censorship')->with('message', 'Kiểm duyệt đơn thành công');
                }
                $dataCusPay['debt'] = $dataCusPay['total_guest_pay'] - $dataCusPay['total_money_invoice'];
                $cusPay->update($dataCusPay);
            }
            $objInvoice->update($data);
            DB::commit();
            return redirect()->route('admin.invoice.censorship')->with('message', 'Kiểm duyệt đơn thành công!');
        } catch (Exception $ex) {
            DB::rollback();
            Log::info($ex);
            return redirect()->route('admin.invoice.censorship')->withErrors('Lỗi hệ thống!');
        }
    }

    /**
     * display khách thanh toán
     * @return [type]
     */
    public function customerPay(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create invoice')) {
            return view('admin.error_permission');
        }
        $dataSearch = [
            'id_cus' => $request->id_cus,
            'customer_name' => $request->customer_name,
            'customer_phone' => $request->customer_phone,
        ];
        $customer = [];
        $cusPays = [];
        $array = true;
        $objCustomer = new Customer();
        if (isset($request->id_cus) && $request->id_cus) {
            $customer = $objCustomer->getCusInvoiceById($request->id_cus);
            $array = false;
            if (!$customer) {
                return redirect()->back()->withErrors('Khồng tìm thấy khách hàng');
            }
        } else {
            if ($request->customer_name || $request->customer_phone) {
                if ($request->type) {
                    if ($request->customer_name) {
                        $customer = $objCustomer->getCusInvoiceByName(Customer::WHOLESALE, $request->customer_name);
                    }
                } else {
                    if ($request->customer_phone) {
                        $customer = $objCustomer->getCusInvoiceByPhone(Customer::RETAIL, $request->customer_phone);
                    $array = false;
                    }
                }
            }
        }
        if (!$array) {
            $objInvoicePay = new InvoicePay();
            $cusPays = $objInvoicePay->getInvoicePayByCus($customer->id);
        }
        if ($array && count($customer) == 1) {
            $customer = $customer->first();
            $array = false;
        }
        $param = [
            'customer' => $customer,
            'array' => $array,
            'cusPays' => $cusPays,
            'dataSearch' => $dataSearch,
        ];
        return view('admin.invoice.customer_pay', $param);
    }

    /**
     * khách hàng thanh toán
     * @param  Request $request
     * @return [type]
     */
    public function customerPayPost(Request $request)
    {
        if (!$request->cus_id || !Customer::find($request->cus_id)) {
            return redirect()->back()->withErrors('Khồng tìm thấy khách hàng');
        }
        $request->cus_pay = (int) str_replace(',','', $request->cus_pay);
        if (empty($request->cus_pay)) {
            return redirect()->back()->withErrors('Chưa nhập số tiền thanh toán');
        }
        $objCusPay = new CustomerInvoicePay();
        $cusPay = $objCusPay->getCustomerPay($request->cus_id);
        if (!$cusPay) {
            return redirect()->back()->withErrors('Khách hàng chưa có hóa đơn');
        }
        DB::beginTransaction();
        try {
            $data = [
                'guest_pay' => $request->cus_pay,
                'user_id' => Auth::id(),
                'customer_id' => $request->cus_id,
            ];
            InvoicePay::create($data);
            $cusPay->total_guest_pay = $request->cus_pay + $cusPay->total_guest_pay;
            $cusPay->debt = $cusPay->total_guest_pay - $cusPay->total_money_invoice;
            $cusPay->update();

            DB::commit();
            return redirect()->back()->with('message', 'Thanh toán thành công!');
        } catch (Exception $ex) {
            DB::rollback();
            Log::info($ex);
            return redirect()->back()->withErrors('Lỗi hệ thống!');
        }
    }
}
