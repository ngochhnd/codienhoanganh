<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
       $this->validator($request->all())->validate();

        try {
            $user = $this->create($request->all());
            return redirect()->back()->with('message', 'Thêm thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return Validator2
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'full_name' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'full_name' => $data['full_name'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'email' => $data['email'],
            'address' => $data['address'],
            'birthday' => $data['birthday'],
        ]);
    }
}
