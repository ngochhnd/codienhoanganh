<?php
namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use function report;
use Exception;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(!Auth::user()->hasPermissionTo('create roles')) {
            return view('admin.error_permission');
        }
        $roles = Role::select('id', 'name', 'created_at')->get();
        $param = [
            'roles' => $roles,
        ];
       return view('admin.roles.index', $param);
    }

    /**
     * [save description]
     * @return [type] [hasPermissionTo: all đặc biệt, thường]
     */
    public function save(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create roles')) {
            return view('admin.error_permission');
        }
        if(!Auth::user()->hasPermissionTo('create roles')) {
            return view('admin.error_permission');
        }
        $validatedData = $request->validate([
            'name' => 'required|unique:roles|max:255',
            ], [
                'name.required' => 'Tên chức vụ không để trống',
                'name.unique'  => 'Tên chức vụ đã tồn tại',
                'name.max'  => 'Tên chức vụ quá dài',
            ]);

        try {
            Role::create(['name' => $request->name]);
            return redirect()->back()->with('message', 'Thêm thành công!');
        } catch (Exception $e) {
            report($e);
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
    }

    /**
     * delete
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        if(!Auth::user()->hasPermissionTo('create roles')) {
            return view('admin.error_permission');
        }
        if(!Auth::user()->hasPermissionTo('create roles')) {
            return view('admin.error_permission');
        }
        $role = Role::find($id);
        if (!$role) {
            return redirect()->back()->withErrors('Không tìm thấy chức vụ!');
        }
        $role->delete();
        return redirect()->back()->with('message', 'Xóa thành công!');
    }

    /**
     * update roles
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function update(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create roles')) {
            return view('admin.error_permission');
        }
        $role = Role::find($request->id);
        if ($role->name == 'sup admin') {
            return redirect()->back()->withErrors('Quyền này không được cập nhật');
        }
        if (!$role) {
            return redirect()->back()->withErrors('Không tìm thấy chức vụ!');
        }
        $validatedData = $request->validate([
                'name' => 'required|max:255|unique:roles,name,' . $request->id,
            ], [
                'name.required' => 'Tên chức vụ không để trống',
                'name.unique'  => 'Tên chức vụ đã tồn tại',
                'name.max'  => 'Tên chức vụ quá dài'
            ]);
        $role->name = $request->name;
        $role->update();
        return redirect()->back()->with('message', 'Cập nhật thành công!');
    }

    /**
     * view page cấp chức vụ cho nhân viên
     * @return [type]
     */
    public function createUser()
    {
        if(!Auth::user()->hasPermissionTo('create role user')) {
            return view('admin.error_permission');
        }

        $user = new User;
        $users = $user->getAll();
        $arrRoleUser = [];
        foreach ($users as $user) {
            $arr = $user->getRolesUser();
            $arrayRole = [];
            if (count($arr)) {
                foreach ($arr as $arrRole) {
                   $arrayRole[] = $arrRole['name'];
                }
            }
            $arrRoleUser[$user->id] = $arrayRole;
        }
        $roles = Role::select('id', 'name')->where('name', '<>', 'sup admin')->get();
        $param = [
            'users' => $users,
            'roles' => $roles,
            'roleUser' => $arrRoleUser,
        ];
        return view('admin.roles.user', $param);
    }

    /**
     * cấp chức vụ cho nhân viên
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function storeUser(Request $request)
    {
        if(!Auth::user()->hasPermissionTo('create role user')) {
            return view('admin.error_permission');
        }
        $validatedData = $request->validate([
                'user_id' => 'required'
            ], [
                'user_id.required' => 'Mã nhân viên không để trống',
            ]);
        $user =  User::find($request->user_id);
        $arrRoles = explode(",",$request->roles);
        if (!$user) {
            return redirect()->back()->withErrors('Nhân viên không tồn tại');
        }
        $userRoleOld = $user->getRolesUser();
        $roles = Role::whereIn('name', $arrRoles)->get();
        try {
            if (count($userRoleOld)) {
                foreach ($userRoleOld as $item) {
                    $user->removeRole($item['name']);
                }
            }
            if (count($arrRoles)) {
                foreach ($roles as $item) {
                    $user->assignRole($item->name);
                }
            }
            return redirect()->back()->with('message', 'Cấp chức vụ cho nhân viên thành công!');
        } catch (Exception $e) {
            report($e);
            return redirect()->back()->withErrors('Có lỗi sảy ra');
        }
    }
}
