<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    const DELETE = 1;
    const ACTIVE = 2;
    const RETAIL = 1; // HD bán lẻ
    const WHOLESALE = 2; // hóa đơn bán sỉ
    const APPROVED = 1;
    const DISAPPROVED = 2;
    const LIMIT = 50;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'customer_id',
        'total_money',
        'guest_pay',
        'status',
        'type',
        'note',
        'deleted_at',
        'user_delete_id',
    ];

    /**
     * Get the user that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User')->select('id', 'name', 'full_name');
    }

    /**
     * Get the customer that owns the comment.
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer')->select('id', 'name', 'phone', 'type');
    }

    /**
     * Get the comments for the blog post.
     */
    public function detail()
    {
        return $this->hasMany('App\InvoiceDetail')
            ->select(
                'invoice_id',
                'unit_id',
                'product_names.name as product_name',
                'price',
                'quantity',
                'discount'
            )
            ->join('product_names', 'product_names.id', 'invoice_details.product_name_id');
    }

    /**
     * lấy thông tin hóa đơn để tin
     * @param  [type] $id
     * @return [type]     [description]
     */
    public function getAll($id)
    {
    	return Invoice::select(
            'invoices.id',
            'invoices.customer_id',
            'invoices.user_id',
            'invoice_details.unit_id as unitId',
            'product_names.name as namePro',
            'invoice_details.price',
            'invoice_details.quantity',
            'invoice_details.discount',
            'invoices.total_money',
            'invoices.guest_pay',
            'invoices.type',
            'customer_invoice_pays.total_money_invoice',
            'customer_invoice_pays.total_guest_pay',
            'customer_invoice_pays.debt',
            'invoices.created_at'
        )
        ->leftJoin('invoice_details', 'invoice_details.invoice_id', '=', 'invoices.id')
        ->leftJoin('customers', 'invoices.customer_id', 'customers.id')
        ->leftJoin('product_names', 'product_names.id', 'invoice_details.product_name_id')
        ->leftJoin('customer_invoice_pays', 'customer_invoice_pays.customer_id', 'customers.id')
        ->where('invoices.id', $id)
        ->get();
    }

    /**
     * lấy thông tin hóa đơn để tin
     * @param  [type] $id
     * @return [type]     [description]
     */
    public function getAllInvoice($id)
    {
    	$invoices = Invoice::select(
            'invoices.id',
            'invoices.total_money',
            'invoices.guest_pay',
            'invoices.note',
            'invoices.id_invoice_debt',
            'invoices.created_at'
        )->get();
    }

    /**
     * lấy hóa đơn cần duyệt hủy
     * @return [type]
     */
    public function getInvoiceCensorship()
    {
        return Invoice::where('status', self::DELETE)->orderBy('invoices.created_at', 'DESC')->paginate(30);
    }

    /**
     * get all trạng thái của hóa đơn
     * @return [type] [description]
     */
    public function getCateInoice()
    {
        return [
            self::RETAIL => 'Bán lẻ',
            self::WHOLESALE => 'Bán sỉ',
        ];
    }

    /**
     * lấy thông tin hóa đơn để tin
     * @param  [type] $id
     * @return [type]     [description]
     */
    public function buildInvoice()
    {
        return Invoice::select(
            'invoices.id',
            'invoices.customer_id',
            'invoices.user_id',
            'invoice_details.unit_id as unitId',
            'product_names.name as namePro',
            'invoice_details.price',
            'invoice_details.quantity',
            'invoices.total_money',
            'invoices.guest_pay',
            'invoices.status',
            'invoices.type',
            'invoices.user_delete_id',
            'customer_invoice_pays.total_money_invoice',
            'customer_invoice_pays.total_guest_pay',
            'customer_invoice_pays.debt',
            'invoices.created_at'
        )
        ->leftJoin('invoice_details', 'invoice_details.invoice_id', '=', 'invoices.id')
        ->leftJoin('customers', 'invoices.customer_id', 'customers.id')
        ->leftJoin('product_names', 'product_names.id', 'invoice_details.product_name_id')
        ->leftJoin('customer_invoice_pays', 'customer_invoice_pays.customer_id', 'customers.id');
    }

    /**
     * lấy thông tin hóa đơn theo id người tạo
     * @param  [type] $userId
     * @return [type]
     */
    public function getByUserCreate($userId)
    {
        $invoiceBuild = $this->buildInvoice();
        return $invoiceBuild->where('invoices.user_id', $userId)->get();
    }

    /**
     * view trạng thái của hóa đơn
     * @param  [type] $invoice [description]
     * @return [type]          [description]
     */
    public function getStatus($invoice)
    {
        $str = '';
        if (!empty($invoice->user_delete_id)) {
            if ($invoice->status == self::ACTIVE) {
                $str = 'Duyệt - Hoạt động';
            } else {
                $str = 'Duyệt - Hủy';
            }
        } else {
            if ($invoice->status == self::ACTIVE) {
                $str = 'Hoạt động';
            } else {
                $str = 'Báo Hủy';
            }
        }
        return $str;
    }

    /**
     * Tính toán giảm giá hóa đơn
     */
    public static function calculationDiscountInvoice($price, $quantity, $discount)
    {
        return ($price * $quantity) * ((100 - $discount) / 100);
    }

    /**
     * Lấy mã hóa đơn theo id hóa đơn
     * @param  [type] $idIvoice
     * @return [type]
     */
    public function getCodeInvoice($idIvoice)
    {
        $string = '0000000000';
        $string = substr($string, 0, (10 - strlen($idIvoice)));
        return 'HĐ: ' . $string . $idIvoice;
    }

    /**
     * Hiển thị hóa đơn index and list
     * @param  array  $dataSearch
     * @param  [type] $empId
     * @return [type]
     */
    public function getListAll($dataSearch = [], $empId = null)
    {
        $collections = Invoice::select(
            "invoices.id",
            "invoices.user_id",
            "invoices.customer_id",
            "invoices.total_money",
            "invoices.guest_pay",
            "invoices.status",
            "invoices.type",
            "invoices.note",
            "invoices.created_at",
            "invoices.note",
            "users.name",
            "users.phone",
            "users.full_name"
            )
            ->join('customers', 'customers.id', '=', 'invoices.customer_id')
            ->leftJoin('users', 'users.id', '=', 'invoices.user_id')
            ->whereNull('invoices.deleted_at');
        if ($empId) {
            $collections = $collections->where('users.id', $empId);
        } elseif (!empty($dataSearch['user_id'])) {
            $collections = $collections->where('users.id', $dataSearch['user_id']);
        } else {

        }
        if (!empty($dataSearch['invoice_id'])) {
            $collections = $collections->where('invoices.id', $dataSearch['invoice_id']);
        }
        if (!empty($dataSearch['customer_name'])) {
            $collections = $collections->where('customers.name', 'LIKE', '%' . $dataSearch['customer_name'] . '%');
        }
        if (!empty($dataSearch['customer_date'])) {
            $date = Carbon::parse($dataSearch['customer_date']);
            $collections = $collections->whereDate('invoices.created_at', '>=' , $date->format('Y-m-d'))
                ->whereDate('invoices.created_at', '<' , $date->addDay()->format('Y-m-d'));
        }
        $collections = $collections->orderBy('invoices.created_at', 'DESC')->paginate(static::LIMIT);
        return $collections->appends($dataSearch);
    }
}
