<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerInvoicePay extends Model
{
    const LIMIT = 50;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'total_money_invoice',
        'total_guest_pay',
        'debt'
    ];

    /**
     * lấy khách hàng theo id
     * @param  [type] $cusId
     * @return [type]
     */
    public function getCustomerPay($cusId)
    {
        return self::where('customer_id', $cusId)->first();
    }

    /**
     * Lấy danh dách khách hàng
     * @return [type] [description]
     */
    public function getCustomerDebt()
    {
        return self::select(
            'customer_id',
            'customers.name',
            'customers.phone',
            'customers.address',
            'customers.type',
            'total_money_invoice',
            'total_guest_pay',
            'debt'
        )
        ->leftJoin('customers', 'customers.id', 'customer_invoice_pays.customer_id')
        ->where('debt', '<', 0)
        ->orderBy('customers.created_at', 'DESC')
        ->paginate(static::LIMIT);
    }
}
