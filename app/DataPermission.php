<?php

namespace App;

class DataPermission
{

    /**
     * set data permission
     * @return [type]
     */
    public function getData()
    {
        return [
            'create invoice' => 'Tạo hóa đơn',
            'manager invoice' => 'Quản lý hóa đơn', // duyệt đơn hủy
            'report invoice' => 'Báo cáo hóa đơn',
            'create customer' => 'Thêm, cập nhật, xóa khách hàng',
            'create user' => 'Thêm, cập nhật, xóa nhân viên', // chung quyền thêm, edit, xóa cùng một quyền
            'create roles' => 'Thêm, cập nhật, xóa chức vụ', // thêm, edit, xóa cùng một quyền
            'create role user' => 'Tạo chức vụ cho nhân viên',
            'create permission role' => 'Tạo quyền cho chức vụ',
            'remove permission role' => 'Xóa quyền của chức vụ', // cài này chỉ có supp admin
        ];
    }
}
