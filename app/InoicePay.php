<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicePay extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'guest_pay',
        'user_id',
    ];

    public function getInvoicePayByCus($idCus)
    {
        return self::select(
            'invoice_pays.id',
            'users.full_name as nameEmp',
            'customers.name as nameCus',
            'invoice_pays.guest_pay',
            'invoice_pays.created_at'
        )
        ->leftJoin('customers', 'customers.id', 'invoice_pays.customer_id')
        ->leftJoin('users', 'users.id', 'invoice_pays.user_id')
        ->where('customer_id', $idCus)
        ->orderBy('invoice_pays.created_at', 'DESC')
        ->get();
    }
}
