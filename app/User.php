<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    const NOT_ACTIVE = 1;
    const ACTIVE = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'full_name', 'password', 'phone', 'email', 'address', 'birthday', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * get all user 
     * @return [collect]
     */
    public function getAll()
    {
        return self::withTrashed()->select('id', 'full_name', 'name', 'phone', 'email', 'address', 'birthday', 'status', 'created_at', 'deleted_at')
            ->with('roles')
            ->orderBy('created_at', 'DESC')
            ->get();
    }
    
    /**
     * get roles of user
     * @return type
     */
    public function getRolesUser()
    {
        $arrRole = [];
        if ($this->roles) {
            foreach ($this->roles as $role) {
                $arrRole[] = [
                    'id' => $role->id,
                    'name' => $role->name,
                ];
            }
        }
        return $arrRole;
    }
    
    /**
     *  mẫu code lây roles va user
     * @return type
     */
    public function report_roles()
    {
        $roles = Role::all();
        $users = User::with('roles')->get();
        $nonmembers = $users->reject(function ($user, $key) {
            return $user->hasRole('Member');
        });
        return view('admin.report_roles', ['roles'=>$roles, 'nonmembers' => $nonmembers]);
    }
}
