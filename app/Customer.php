<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Customer extends Model
{
    use HasRoles;

    const RETAIL = 1; //ban le
    const WHOLESALE = 2;
    const LIMIT = 50;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'type', 'address', 'deleted_at'
    ];
    
    /**
     * get first khách hàng
     * @param  [type] $name
     * @param  [type] $phone
     * @return [type]
     */
    public function findCus($name, $phone)
    {
        return self::select('id', 'name', 'phone', 'address')
            ->where('name', $name)
            ->where('phone', $phone)
            ->first();
    }

    /**
     * get all customer 
     * @return [collect]
     */
    public function getAll($dataSearch)
    {
        $collect = self::select('id', 'name', 'phone', 'address', 'type', 'created_at');
        if (!empty($dataSearch['customer_name'])) {
            $collect->where('name', 'LIKE', '%' . $dataSearch['customer_name'] . '%');
        }
        if (!empty($dataSearch['customer_phone'])) {
            $collect->where('phone', 'LIKE', '%' . $dataSearch['customer_phone'] . '%');
        }
        $collect = $collect->orderBy('type', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->paginate(static::LIMIT)
            ->appends($dataSearch);
        return $collect;
    }

    /**
     * tìm kiếm khách hàng bán lẻ
     * @return [type] [description]
     */
    public function searchCusRetails($name, $phone = '')
    {
        return self::where('name', 'LIKE', '%' . $name . '%')
            ->where('phone', $phone)
            ->where('type', self::RETAIL)
            ->get(['id', 'name as text']);
    }

    /**
     * tìm kiếm khách hàng bán sỉ
     * @return [type] [description]
     */
    public function searchCusWholesale($name)
    {
        return  self::where('name', 'LIKE', '%' . $name . '%')
            ->where('type', self::WHOLESALE)
            ->get(['id', 'name as text']);
    }

    /**
     * lấy thông tin hóa đơn nợ của nhân viên
     * @param  [int] $id
     * @return [object]
     */
    public function getCusInvoiceById($id)
    {
        return self::select(
            'customers.id',
            'customers.name',
            'customers.phone',
            'customers.address',
            'customers.type',
            'customer_invoice_pays.total_money_invoice',
            'customer_invoice_pays.total_guest_pay',
            'customer_invoice_pays.debt'
        )
        ->leftJoin('customer_invoice_pays', 'customer_invoice_pays.customer_id', 'customers.id')
        ->where('customers.id', $id)
        ->first();
    }

    /**
     * lấy thông tin hóa đơn nợ của nhân viên - hóa đơn còn hoạt động
     * @param  [int] $type
     * @param  [int] $phone
     * @return [object]
     */
    public function getCusInvoiceByPhone($type, $phone)
    {
        return self::select(
            'customers.id',
            'customers.name',
            'customers.phone',
            'customers.address',
            'customers.type',
            'customer_invoice_pays.total_money_invoice',
            'customer_invoice_pays.total_guest_pay',
            'customer_invoice_pays.debt'
        )
        ->leftJoin('customer_invoice_pays', 'customer_invoice_pays.customer_id', 'customers.id')
        ->where('customers.type', $type)
        ->where('customers.phone', $phone)
        ->first();
    }

    /**
     * lấy thông tin hóa đơn nợ của nhân viên - hóa đơn còn hoạt động
     * @param  [int] $type
     * @param  [int] $phone
     * @return [object]
     */
    public function getCusInvoiceByName($type, $name)
    {
        return self::select(
            'customers.id',
            'customers.name',
            'customers.phone',
            'customers.address',
            'customers.type',
            'customer_invoice_pays.total_money_invoice',
            'customer_invoice_pays.total_guest_pay',
            'customer_invoice_pays.debt'
        )
        ->leftJoin('customer_invoice_pays', 'customer_invoice_pays.customer_id', 'customers.id')
        ->where('customers.type', $type)
        ->where('customers.name', 'LIKE', '%' . $name . '%')
        ->get();
    }

    /**
     * lấy thông tin hóa đơn nợ của nhân viên theo loại hóa đơn
     * @param  [int] $type
     * @return [object]
     */
    public function getCusInvoiceByType($type)
    {
        return self::select(
            'customers.id',
            'customers.name',
            'customers.phone',
            'customers.address',
            'customers.type',
            'customer_invoice_pays.total_money_invoice',
            'customer_invoice_pays.total_guest_pay',
            'customer_invoice_pays.debt'
        )
        ->leftJoin('customer_invoice_pays', 'customer_invoice_pays.customer_id', 'customers.id')
        ->where('customers.type', $type)
        ->orderBy('customers.created_at', 'DESC')
        ->paginate(static::LIMIT);
    }
}
