<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id', 'unit_id', 'product_name_id', 'price', 'quantity', 'discount'
    ];

    /**
     * Tên đơn vị mặt hàng
     * @return [type] [description]
     */
    public function nameUnit()
    {
    	return ['cái', 'chiếc', 'đơn vị khác'];
    }
}
