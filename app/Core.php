<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Core extends Model
{
    public static function getSTT($collection)
    {
        $perPage = $collection->perPage();
        $currentPage = $collection->currentPage();
        return $currentPage * $perPage - $perPage;
    }
}
