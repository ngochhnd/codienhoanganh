const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
// mix.js('resources/js/style.js', 'public/js');
// mix.js('resources/js/print/print_order.js', 'public/js/print');
mix.js('resources/js/invoice/create.js', 'public/js/invoice');
// mix.js('resources/js/invoice/modal_invoice_deatail.js', 'public/js/invoice');
// mix.js('resources/js/user/index.js', 'public/js/user');
// mix.js('resources/js/permission/index.js', 'public/js/permission');

// mix.sass('resources/sass/app.scss', 'public/css');
// mix.sass('resources/sass/invoice/invoice.scss', 'public/css/invoice');
// mix.sass('resources/sass/admin/style.scss', 'public/admin');