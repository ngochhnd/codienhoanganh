@extends('admin.layouts.default')

<?php
    use App\DataPermission;
    use Carbon\Carbon;

    $objPer = new DataPermission();
    $arrDataPers = $objPer->getData();
?>
@section('css')
@endsection

@section('content')
<div class="row">
    <div class="col-md-9">
        <h4>Danh sách chức vụ:</h4>
        <div class="table-responsive">
            <table class="table table-hover table-striped view-user">
                <thead class="bg-success">
                    <tr class="">
                        <th scope="col">STT</th>
                        <th scope="col">Tên chức vụ</th>
                        <th scope="col">Ngày tạo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($roles))
                        <?php $i = 0?>
                        @foreach($roles as $role)
                            @if ($role->name !== 'sup admin')
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td class="name" data-id="{{ $role->id}}">{{ $role->name }}</td>
                                    <td>{{ Carbon::parse($role->created_at)->format("d-m-Y") }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary edit-role" title="sửa thông tin"><i class="fas fa-pen"></i></button>
                                        <a class="btn btn-danger delete-message-name cursor-pointer" title="Xóa chức vụ"
                                            data-placement="right" data-name="{{ $role->name }}"
                                            data-message="Bạn có muốn xóa quyền: "
                                            style="color: #ffff"><i class="far fa-trash-alt"></i>
                                            <form class="delete" action="{{ route('admin.role.delete', ['id' => $role->id]) }}" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                {{ csrf_field() }}
                                            </form>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
     <div class="col-md-3">
        <h4>Thêm chức vụ mới</h4>
        <div class="role-form margin-bottom-30">
            <form action="{{route('admin.role.save')}}" method="post">
                @csrf
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Nhập tên chức vụ" required>
                  </div>
                <button type="submit" class="btn btn-primary text-center">Thêm chức vụ mới</button>
            </form>
        </div>
        <hr>
        <h4 class="margin-top-30">Cập nhật chức vụ</h4>
        <div class="role-form">
            <form action="{{route('admin.role.update') }}" method="post">
                @csrf
                <div class="form-group">
                    <input type="number" class="form-control" name="id" id="id-update-role" hidden>
                    <input type="text" class="form-control" name="name" id="name-update-role" required>
                  </div>
                <button type="submit" class="btn btn-info text-center">Cập nhật chức vụ</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(".edit-role").on('click', function() {
        var name = $(this).closest('tr').find('.name').text();
        var id = $(this).closest('tr').find('.name').attr('data-id');
        $("#name-update-role").val(name);
        $("#id-update-role").val(id);
    })
</script>
@endsection
