@extends('admin.layouts.default')

<?php
    use App\DataPermission;

    $objPer = new DataPermission();
    $arrDataPers = $objPer->getData();
?>
@section('css')
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <h4>Nhân viên trong hệ thống</h4>
            <ul class="list-group list-roles">
                @if (count($users))
                    <?php $i = -1 ; $userActive = ''?>
                    @foreach($users as $user)
                        <?php
                           $i++;
                            $active = '';
                            if ($i == 0) {
                                $active = 'active';
                                $userActive = $user->id;
                            }
                        ?>
                        <li class="list-group-item {{ $active }}"
                            data-roles-id="{{ $user->id }}"
                            data-pre="{{ array_key_exists($user->id, $roleUser) ? json_encode($roleUser[$user->id]) : ''}}"
                            >{{ $user->full_name }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-md-8 boder-left">
            <h4>Chức vụ trong hệ thống</h4>
            <ul class="list-group list-group-flush list-permission">
                @if (count($roles) && count($users))
                    @foreach($roles as $role)
                        <li class="list-group-item">
                            <!-- Default checked -->
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="check{{ $role->id }}" value="{{ $role->name }}" data-name="{{ $role->name }}"
                                <?php
                                    if ($userActive !== '' &&
                                        array_key_exists($userActive, $roleUser) &&
                                        in_array($role->name, $roleUser[$userActive])) {
                                        echo "checked";
                                    }
                                ?>
                                >
                                <label class="custom-control-label" for="check{{ $role->id }}">{{ $role->name }}</label>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-12 text-center" style="margin-top: 20px">
            <button type="button" class="btn btn-primary btn-modal-permission" data-toggle="modal" data-target="#myModal">
                Cấp chức vụ cho nhân viên
            </button>
        </div>
    </div>
</div>
<!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Lưu chức vụ cho nhân viên</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <form action="{{ route('admin.role.store.user') }}" method="post">
            @csrf
            <div class="modal-body">
                <input type="number" name="user_id" value="" id="id-role" hidden>
                <input type="text" value="" id="arr-per" name="roles" hidden>
                    Nhân viên: <b><span class="name-role"></span></b> có những chức vụ như sau:
                    <br>
                <ul class="md-list-per">
                </ul>
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Lưu <i class="far fa-save"></i></button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@section('script')
    <script src="{{ asset('js/permission/index.js') }}"></script>
@endsection
