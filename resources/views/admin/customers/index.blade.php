@extends('admin.layouts.default')

@section('css')
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection
<?php
    use Carbon\Carbon;
    use App\Core;
    use App\customer;
?>

@section('content')
<div class="">
    <form action="{{ route('admin.customer.index') }}" method="get" class="form-inline" >
        <div class="row" style="width: 100%">
            <div class="col-md-6">
                <a href="{{ route('admin.customer.create') }}" class="btn btn-success mb-4" role="button" target="_bank">
                    <i class="fas fa-plus"></i> Thêm khách hàng mới
                </a>
            </div>
             <div class="col-md-6 text-right">
                <a href="{{route('admin.customer.index')}}" class="btn btn-info" role="button">Refresh tìm kiếm</a>
                <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> tìm kiếm</button>
            </div>
        </div>
        <table class="table table-hover table-striped view-customer">
            <thead class="bg-success">
                <tr class="">
                    <th scope="col">STT</th>
                    <th scope="col">Tên khách hàng</th>
                    <th scope="col">Số điện thoại</th>
                    <th scope="col">Địa chỉ</th>
                    <th scope="col">Khách hàng</th>
                    <th scope="col">Ngày tạo</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>
                        <input type="search" name="customer_name"  class="form-control" style="width: 100%"
                        value="{{ array_key_exists('customer_name', $dataSearch) ? $dataSearch['customer_name'] : ''}}" placeholder="search tên khách hàng">
                    </td>
                    <td>
                        <input type="number" name="customer_phone"  class="form-control" style="width: 100%" placeholder="search số điện thoại" value="{{ array_key_exists('customer_phone', $dataSearch) ? $dataSearch['customer_phone'] : ''}}"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $i = Core::getSTT($customers) ?>
                @foreach($customers as $key => $customer)
                <tr>
                    <th scope="row">{{ ++$i }}</th>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->phone }}</td>
                    <td>{{ $customer->address }}</td>
                    <td>
                        @if ($customer->type == Customer::WHOLESALE)
                            <span class="badge badge-pill badge-success">Bán sỉ</span>
                        @else
                            <span class="badge badge-pill badge-primary">Bán lẻ</span>
                        @endif
                    </td>
                    <td>{{ Carbon::parse($customer->created_at)->format("d-m-Y") }}</td>
                    <td class="view-pen-trash">
                        <a href="{{ route('admin.customer.edit', ['id' => $customer->id]) }}" class="btn btn-primary"data-toggle="tooltip" title="sửa thông tin" data-placement="left" role="button"><i class="fas fa-pen"></i>
                        </a>
                        @if (!$customer->hasRole('sup admin'))
                        <a class="btn btn-danger delete-message-name" data-toggle="tooltip" title="khách hàng" data-placement="right"
                            data-name="{{ $customer->name }}" style="color: #ffff"
                            data-message="Bạn có muốn xóa khách hàng: "><i class="far fa-trash-alt"></i>
                            <form class="delete" action="{{ route('admin.customer.delete', ['id' => $customer->id]) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </form>
    <div class="text-center">
        {{ $customers->links() }}
    </div>
</div>

@endsection

@section('script')
    <script src="{{ asset('js/user/index.js') }}"></script>
@endsection
