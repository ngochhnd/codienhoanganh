@extends('admin.layouts.default')

@section('css')

@endsection
<?php
    use App\User;
    use App\Invoice;

    $objInvoice = new Invoice();
    $invoiceCate = $objInvoice->getCateInoice();
?>

@section('content')
<h3 class="mb-4">Danh sách khách hàng nợ</h3>
<div class="table-responsive">
    <table class="table table-hover table-striped table-bordered text-center">
        <thead class="bg-success">
            <tr class="">
                <th scope="col" class="with-10">STT</th>
                <th scope="col">Tên khách hàng</th>
                <th scope="col">SĐT</th>
                <th scope="col">Địa chỉ</th>
                <th scope="col">Khách hàng</th>
                <th scope="col">Tổng hóa đơn</th>
                <th scope="col">Tổng thanh toán</th>
                <th scope="col">Tiền nợ</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @php $i = 0; @endphp
            @foreach($customers as $key => $customer)
            <tr>
                <th scope="row">{{ ++$i }}</th>
                <td class="text-left">{{ $customer->name }}</td>
                <td>{{ $customer->phone }}</td>
                <td>{{ $customer->address }}</td>
                <td>{{ $invoiceCate[$customer->type] }}</td>
                <td class="text-center">{{ number_format($customer->total_money_invoice) }}</td>
                <td class="text-center">{{ number_format($customer->total_guest_pay) }}</td>
                <td class="text-center">{{ number_format($customer->debt) }}</td>
                <td>
                    <a href="#" class="btn btn-primary" title="xem chi tiết hóa đơn"role="button" target="_bank"><i class="fas fa-eye"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="text-center">
        {{ $customers->links() }}
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/user/index.js') }}"></script>
@endsection
