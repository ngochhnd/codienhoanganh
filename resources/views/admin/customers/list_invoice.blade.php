@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;

    $objInvoice = new Invoice();
    $totalMoney = 0;
    $totalGuestPay = 0;
    $invoice = $invoices->first();
?>

@section('content')
<div id="list_invoice">
    <form action="{{route('admin.invoice.list')}}" method="get" class="form-inline" >
        <div class="text-right" style="width: 100%; margin-bottom: 10px">
            <h5 class="text-left">Danh sách hóa đơn KH: {{ $customer->name }}</h5>
            @if ($invoice)
                <a href="{{route('admin.customer.history_pays', ['id' => $invoice->id])}}">Lịch sử thanh toán</a>
            @endif
        </div>
        <table class="table table-bordered ">
            <thead>
                <tr class="text-center bg-info">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col">Mã HD</th>
                    <th scope="col">Tên nhân viên</th>
                    <th scope="col">Ngày bán</th>
                    <th scope="col">Loại HĐ</th>
                    <th scope="col">Tổng hóa đơn</th>
                    <th scope="col">Thanh toán</th>
                    <th scope="col">Ghi chú</th>
                    <th scope="col">Trạng thái</th>
                </tr>
            </thead>
            <tbody>
                @if (count($invoices))
                    <?php $i = Core::getSTT($invoices) ?>
                    @foreach($invoices as $invoice)
                        <tr>
                            <th scope="row" class="text-center">{{ ++$i }}</th>
                            <td class="text-center width-150"> {{ $objInvoice->getCodeInvoice($invoice->id) }}</td>
                            <td>{{ $invoice->user->full_name }}</td>
                            <td class="text-center">{{ Carbon::parse($invoice->created_at)->format("d-m-Y H:i") }}</td>
                            <td class="text-center">
                                @if ($invoice->type == Invoice::WHOLESALE)
                                    <span class="badge badge-pill badge-success">Bán sỉ</span>
                                @else
                                    <span class="badge badge-pill badge-primary">Bán lẻ</span>
                                @endif
                            </td>

                            <td>{{ number_format($invoice->total_money) }}</td>
                            <td>{{ number_format($invoice->guest_pay) }}</td>
                            <td class="max-with-md-300">{{ $invoice->note }}</td>
                            <td class="text-center">
                                <?php $status = $objInvoice->getStatus($invoice); ?>
                                @if ($invoice->status == Invoice::ACTIVE)
                                    <span class="badge badge-success">{{ $status }}</span>
                                @else
                                    <span class="badge badge-danger">{{ $status }}</span>
                                @endif
                            </td>
                            <?php
                                $totalMoney += $invoice->total_money;
                                $totalGuestPay += $invoice->guest_pay;
                            ?>
                        </tr>
                    @endforeach
                        <td>#</td>
                        <td colspan="4">Tổng tiền: </td>
                        <td>{{ number_format($totalMoney) }}</td>
                        <td>{{ number_format($totalGuestPay) }}</td>
                        <td colspan="2"></td>
                @endif
            </tbody>
        </table>
    </form>
    <div class="text-center">
        {{ $invoices->links() }}
    </div>
</div>
@endsection

@section('script')
@endsection
