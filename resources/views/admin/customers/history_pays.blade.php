@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;

    $objInvoice = new Invoice();
    $totalMoney = 0;
    $totalGuestPay = 0;
?>

@section('content')
<div id="history-pays">
    <h5 class="mt-3">Lịch sử trả tiền nợ của khách hàng:</h5>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>NV thanh toán</th>
                    <th>Số tiền</th>
                    <th>Ngày thanh toán</th>
                </tr>
            </thead>
            <tbody>
            @if (count($cusPays))
                @foreach($cusPays as $item)
                  <tr>
                    <td>{{ $item->nameEmp }}</td>
                    <td>{{ number_format($item->guest_pay) }}</td>
                    <td>{{ Carbon::parse($item->created_at)->format('d-m-Y') }}</td>
                  </tr>
                @endforeach
                @endif
            </tbody>
          </table>
</div>
@endsection

@section('script')
@endsection
