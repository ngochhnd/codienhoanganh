@extends('admin.layouts.default')

<?php
    use App\DataPermission;

    $objPer = new DataPermission();
    $arrDataPers = $objPer->getData();
?>
@section('css')
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <h4>Chức vụ trong hệ thống</h4>
            <ul class="list-group list-roles">
                @if (count($arrRoles))
                    <?php $i = -1 ; $roleActive = ''?>
                    @foreach($arrRoles as $role)
                        <?php
                            $i++;
                            $active = '';
                            if ($i == 0) {
                                $active = 'active';
                                $roleActive = $role->id;
                            }
                        ?>
                        <li class="list-group-item {{ $active }} {{$roleActive}}"
                            data-roles-id="{{ $role->id }}"
                            data-pre="{{ array_key_exists($role->id, $arrPreRoleName) ? json_encode($arrPreRoleName[$role->id]) : ''}}"
                            >{{ $role->name }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-md-8 boder-left">
            <h4>Quyền tương ứng của chức vụ</h4>
            <ul class="list-group list-group-flush list-permission">
                @if (count($arrPres) && count($arrRoles))
                    @foreach($arrPres as $arrPre)
                        <li class="list-group-item">
                            <!-- Default checked -->
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="check{{ $arrPre->id }}" value="{{ $arrPre->name }}" data-name="{{ $arrPre->name }}"
                                <?php
                                    if ($roleActive !== '' &&
                                        array_key_exists($roleActive, $arrPreRoleName) &&
                                        in_array($arrPre->name, $arrPreRoleName[$roleActive])) {
                                        echo "checked";
                                    }
                                ?>
                                >
                                <label class="custom-control-label" for="check{{ $arrPre->id }}">{{ $arrDataPers[$arrPre->name] }}</label>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-12 text-center" style="margin-top: 20px">
            <button type="button" class="btn btn-primary btn-modal-permission" data-toggle="modal" data-target="#myModal">
                Cấp quyền
            </button>
        </div>
    </div>
</div>
<!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Thông tin lưu quyền cho nhân viên</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <form action="{{ route('admin.permission.update') }}" method="post">
            @csrf
            <div class="modal-body">
                <input type="number" name="role_id" value="" id="id-role" hidden>
                <input type="text" value="" id="arr-per" name="permission" hidden>
                    Chức vụ: <b><span class="name-role"></span></b> có những quyền như sau:
                    <br>
                <ul class="md-list-per">
                </ul>
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Lưu <i class="far fa-save"></i></button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@section('script')
    <script src="{{ asset('js/permission/index.js') }}"></script>
@endsection
