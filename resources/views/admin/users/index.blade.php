@extends('admin.layouts.default')

@section('css')
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection
<?php
    use App\User;
?>

@section('content')
<a href="{{ route('admin.user.create') }}" class="btn btn-success mb-4" role="button" target="_bank">
    <i class="fas fa-plus"></i> Thêm mới nhân viên
</a>
<div class="table-responsive">
    <table class="table table-hover table-striped view-user">
        <thead class="bg-success">
            <tr class="">
                <th scope="col">STT</th>
                <th scope="col">Tên nhân viên</th>
                <th scope="col">Tên đăng nhập</th>
                <th scope="col">Số điện thoại</th>
                <th scope="col">Chức vụ</th>
                <th scope="col">Trạng thái</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @php $i = 0; @endphp
            @foreach($users as $key => $user)
            <tr>
                <th scope="row">{{ ++$i }}</th>
                <td>{{ $user->full_name }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->phone }}</td>
                <td>
                    @php
                        $userRoles = $user->getRolesUser();
                        if (count($userRoles)) {
                            foreach ($userRoles as $role) {
                                echo ' <span class="badge badge-secondary"> ' . $role['name'] . '</span> ';
                            }
                        }
                    @endphp
                </td>
                <td>
                    @if (empty($user->deleted_at))
                    <span class="badge badge-success">Hoạt động</span>
                    @else
                    <span class="badge badge-danger">Đóng</span>
                    @endif
                </td>
                <td class="view-pen-trash">
                    <button type="button" class="btn btn-info modal-view" title="Xem chi tiết" data-toggle="modal" data-target="#myModal" data-row="{{ $users[$key] }}"><i class="fas fa-eye"></i>
                    </button>
                    <a href="{{ route('admin.user.edit', ['id' => $user->id]) }}" class="btn btn-primary"data-toggle="tooltip" title="sửa thông tin" data-placement="left" role="button" target="_bank"><i class="fas fa-pen"></i>
                    </a>
                    @if (!$user->hasRole('sup admin'))
                    <a class="btn btn-danger delete-message-name" data-toggle="tooltip" title="Xóa nhân viên" data-placement="right"
                        data-name="{{ $user->name }}" style="color: #ffff"
                        data-message="Bạn có muốn xóa nhân viên: "><i class="far fa-trash-alt"></i>
                        <form class="delete" action="{{ route('admin.user.delete', ['id' => $user->id]) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                        </form>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="text-center">
        {{-- {{ $users->links() }} --}}
    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title text-center">Thông tin nhân viên</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 modal-col1">
                    </div>
                    <div class="col-sm-6 modal-col2">
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/user/index.js') }}"></script>
@endsection
