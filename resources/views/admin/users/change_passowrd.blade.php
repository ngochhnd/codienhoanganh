@extends('admin.layouts.default')

@section('css')

@endsection
<?php
    use App\User;
?>
@section('content')
<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Thay đổi mật khẩu</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.user.save_change_pass') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="passwordOld" class="col-md-4 col-form-label text-md-right">Mật khẩu hiện tại:<sup class="red">*</sup> </label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" required name="passwordOld">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="passwordNew" class="col-md-4 col-form-label text-md-right">Mật khẩu mới:<sup class="red">*</sup> </label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" required name="passwordNew">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="passwordNew" class="col-md-4 col-form-label text-md-right">Nhập lại mật khẩu mới:<sup class="red">*</sup> </label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" required name="passwordAgain">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Lưu thay đổi') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
   <div class="container">
       
   </div>
@endsection
