<?php
    $userAuth = auth()->user();

    $hoaDon = request()->is('create-order') ||
        request()->is('qlch/invoice/list') ||
        request()->is('qlch/invoice/customer-pay');

    $QLHoaDon = request()->is('qlch/invoice/censorship') ||
        request()->is('qlch/invoice/index');

    $report = request()->is('qlch/invoice/report') ||
        request()->is('qlch/customer-debt') ||
        request()->is('qlch/customer/report/retail') ||
        request()->is('qlch/customer/*/list-invoice') ||
        request()->is('qlch/customer/history-pays/*') ||
        request()->is('qlch/customer/report/wholesale');

    $customer = request()->is('qlch/customers') ||
        request()->is('qlch/customers/create') ||
        request()->is('qlch/customer-edit/*');

    $user = request()->is('qlch/user/index') ||
        request()->is('qlch/user/create');

    $permission = request()->is('qlch/roles/create/user') ||
        request()->is('qlch/roles') ||
        request()->is('qlch/permission');
?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('admin.index') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Hoàng Ánh</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
    
    @if($userAuth->can('create invoice') ||
        $userAuth->can('manager invoice') ||
        $userAuth->can('report invoice'))
    <li class="nav-item">
        <a class="nav-link {{ $hoaDon ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-file-invoice"></i>
            <span>Hóa đơn</span>
        </a>
        <div id="collapseTwo" class="collapse {{ $hoaDon ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('create-order') }}">Tạo hóa đơn</a>
                <a class="collapse-item" href="{{ route('admin.invoice.list') }}">Danh sách hóa đơn</a>
                <a class="collapse-item" href="{{ route('admin.invoice.customer_pay') }}">Khách hàng thanh toán</a>
            </div>
        </div>
    </li>
    @endif

    @if($userAuth->can('manager invoice'))
        <li class="nav-item">
            <a class="nav-link {{ $QLHoaDon ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#managerInvoice" aria-expanded="true" aria-controls="managerInvoice">
                <i class="fas fa-tasks"></i>
                <span>Quản lý hóa đơn</span>
            </a>
            <div id="managerInvoice" class="collapse {{ $QLHoaDon ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.invoice.censorship') }}">Hóa đơn cần duyệt</a>
                    <a class="collapse-item" href="{{ route('admin.invoice.index') }}">Danh sách hóa đơn</a>
                </div>
            </div>
        </li>
    @endif
    
    @if($userAuth->can('create customer'))
        <li class="nav-item">
            <a class="nav-link {{ $customer ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#manageCustomer" aria-expanded="true" aria-controls="manageCustomer">
                <i class="fas fa-users"></i>
                <span>Quản lý khách hàng</span>
            </a>
            <div id="manageCustomer" class="collapse {{ $customer ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.customer.index') }}">Danh sách khách hàng</a>
                    <a class="collapse-item" href="{{ route('admin.customer.create') }}">Thêm khách hàng</a>
                </div>
            </div>
        </li>
    @endif

    @if($userAuth->can('report invoice'))
        <li class="nav-item">
            <a class="nav-link {{ $report ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#report" aria-expanded="true" aria-controls="report">
                <i class="fas fa-chart-bar"></i>
                <span>Thống kê - báo cáo</span>
            </a>
            <div id="report" class="collapse {{ $report ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.invoice.report') }}">Báo cáo hóa đơn</a>
                    {{-- <a class="collapse-item" href="{{ route('admin.invoice.report.debt') }}">Hóa đơn còn nợ</a> --}}
                    <a class="collapse-item" href="{{ route('admin.customer.debt') }}">Khách hàng còn nợ</a>
                    <a class="collapse-item" href="{{ route('admin.customer.report.wholesale') }}">Khách hàng bán sỉ</a>
                    <a class="collapse-item" href="{{ route('admin.customer.report.retail') }}">Khách hàng bán lẻ</a>
                </div>
            </div>
        </li>
    @endif

    @if($userAuth->can('create user'))
    <li class="nav-item">
        <a class="nav-link {{ $user ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
           <i class="fas fa-users-cog"></i>
            <span>Quản lý nhân viên</span>
        </a>
        <div id="collapseUtilities" class="collapse {{ $user ? 'show' : '' }}" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.user.index')}}">Danh sách nhân viên</a>
                <a class="collapse-item" href="{{ route('admin.user.create')}}">Thêm nhân viên</a>
            </div>
        </div>
    </li>
    @endif

    @if($userAuth->can('create roles') ||
        $userAuth->can('create role user') ||
        $userAuth->can('create permission role'))
    <li class="nav-item">
        <a class="nav-link {{ $permission ? '' : 'collapsed' }}" href="#" data-toggle="collapse" data-target="#permission" aria-expanded="true" aria-controls="permission">
            <i class="fas fa-user-cog"></i>
            <span>Phân quyền</span>
        </a>
        <div id="permission" class="collapse {{ $permission ? 'show' : '' }}" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @can('create role user')
                    <a class="collapse-item" href="{{ route('admin.role.create_user')}}">Cấp chức vụ nhân viên</a>
                @endcan
                @can('create roles')
                    <a class="collapse-item" href="{{ route('admin.role.index')}}">Chức vụ hệ thống</a>
                @endcan
                @can('create permission role')
                    <a class="collapse-item" href="{{ route('admin.permission.index')}}">Phân quyền</a>
                @endcan
            </div>
        </div>
    </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>