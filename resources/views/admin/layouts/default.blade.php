<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hoàng Ánh</title>
    <!-- Custom fonts for this template-->
    <link type="text/css" href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Custom styles for this template-->
    <link href="{{ asset('admin/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href='{{ asset('admin/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}' rel='stylesheet' type='text/css'>
    <script src='{{ asset('admin/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}' type='text/javascript'></script>

    <link rel="stylesheet" href="{{ asset('admin/style.css') }}">
    @yield('css')
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
            <!-- Sidebar -->
            @include('admin.layouts.nav')
            <!-- End of Sidebar -->
             
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">            
                <!-- Main Content -->
                <div id="content">

                    @include('admin.layouts.top_body')
                
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        {{-- ====== message ======== --}}
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @if($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $error}}
                                </div>
                            @endforeach
                        @endif
                        {{-- ======================= --}}
                        @yield('content')
                    </div>
                </div>

                <!-- Footer -->
                @include('admin.layouts.footer')
                <!-- End of Footer -->    
            </div>
            <!-- End of Content Wrapper -->
    </div>

        <!-- End of Content Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('admin/js/sb-admin-2.min.js') }}"></script>

    <script src="{{ asset('js/style.js') }}"></script>
    @yield('script')
</body>

</html>