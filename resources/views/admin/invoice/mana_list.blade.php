@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;

    $objInvoice = new Invoice();
?>

@section('content')
<h4 class="mb-3">Hóa đơn cần duyệt</h4>
<div id="list_invoice">
    <form action="{{route('admin.invoice.index')}}" method="get" class="form-inline" >
        <table class="table table-bordered table-invoice">
            <thead>
                <tr class="text-center bg-primary">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col" style="width: 80px;">Mã HD</th>
                    <th scope="col">Tên khách hàng</th>
                    <th scope="col">Tên nhân viên</th>
                    <th scope="col">Ngày bán</th>
                    <th scope="col">Loại HĐ</th>
                    <th scope="col">Ghi chú</th>
                    <th scope="col">Trạng thái</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if (count($invoices))
                    <?php $i = Core::getSTT($invoices) ?>
                    @foreach($invoices as $invoice)
                        <tr>
                            <th scope="row" class="text-center">{{ ++$i }}</th>
                            <td class="text-center">{{ $invoice->id }}</td>
                            <td>{{ $invoice->customer->name }}</td>
                            <td>{{ $invoice->user->full_name }}</td>
                            <td>{{ Carbon::parse($invoice->created_at)->format("d-m-Y H:i") }}</td>
                            <td class="text-center">
                                @if ($invoice->type == Invoice::WHOLESALE)
                                    <span class="badge badge-success">Bán sỉ</span>
                                @else
                                    <span class="badge badge-primary">Bán lẻ</span>
                                @endif
                            </td>
                            <td class="max-with-md-300">{{ $invoice->note }}</td>
                            <td>
                                <?php $status = $objInvoice->getStatus($invoice); ?>
                                @if ($invoice->status == Invoice::ACTIVE)
                                    <span class="badge badge-success">{{ $status }}</span>
                                @else
                                    <span class="badge badge-danger">{{ $status }}</span>
                                @endif
                            </td>
                            <td class="eye-pen">
                                <button type="button" class="btn btn-info modal-view" title="Xem chi tiết" data-toggle="modal" data-target="#myModal"
                                data-invoice="{{ json_encode([$invoice->total_money,$invoice->guest_pay]) }}"
                                data-detail="{{ $invoice->detail }}"><i class="fas fa-eye"></i>
                                </button>
                                <button type="button" class="btn btn-primary censorship" title="duyệt hóa đơn" data-toggle="modal" data-target="#myModalCensorship" data-id="{{ $invoice->id }}"><i class="fas fa-pen"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </form>
    <div class="text-center">
        {{ $invoices->links() }}
    </div>

    <!-- The Modal -->
    @include('admin.invoice.modals.invoice_detail')

    <!-- The Modal -->
    <div class="modal fade" id="myModalCensorship">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                      <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title text-center">Duyệt hủy hóa đơn</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body text-center">
                    <form action="{{ route('admin.invoice.is-censorship') }}"  method="post">
                        {{ csrf_field() }}
                        <input type="number" name="id" id="modal-id" hidden>
                        <div class="form-check-inline">
                          <label class="form-check-label" for="check1">
                            <input type="radio" class="form-check-input" id="check1" name="status" value="1" checked> Duyệt
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label" for="check2">
                            <input type="radio" class="form-check-input" id="check2" name="status" value="2">Không duyệt
                          </label>
                        </div>
                        <br><br>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Lưu</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#list_invoice').on('click', "form .huy-don", function (argument) {
            $("#myModalDelte").find('input[name="id"]').val($(this).attr('data-id'));
        })
        $('table').on('click', '.censorship', function() {
            $('#modal-id').val($(this).attr('data-id'));
        })
    </script>
@endsection
