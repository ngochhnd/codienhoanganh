<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hoàng Ánh</title>

        <link href="{{ asset('admin/css/sb-admin-2.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('admin/style.css') }}">
        <style>
            center {
                padding-top: 10px;
            }
            table.print,
            table.print th,
            table.print td {
                border-style: dashed;
            }
            .top-invoice {
                display: -webkit-flex; /* Safari */
                  -webkit-justify-content: flex-start; /* Safari 6.1+ */
                  display: flex;
                  justify-content: flex-start;
            }
            .top-invoice .text-left {
                padding-right: 15px;
            }
            h4, p {
                margin-bottom: 0px;
            }
            hr {
                margin: 0px;
            }
            body {
                font-size: 14px;
            }
            table th,
            table td {
                padding:5px !important;
            }
        </style>
</head>
<?php
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Auth;
    use App\Invoice;

    $objInvoice = new Invoice();
    $invoiceCate = $objInvoice->getCateInoice();
    $invoice = $invoices->first();
?>
<body>
    <div class="container-fluid">
        <center>
            <div class=top-invoice>
                <div class="text-left" style="width: 75%;">
                    <h4 style="font-weight: bold; text-align: center;"><sup style="font-weight: 400;font-size: 16px;">CƠ ĐIỆN</sup> HOÀNG ÁNH</h4>
                    <p><i>Cung Cấp:</i></p>
                    <p>MOTEUR KÉO BỚM NƯỚC, BƠM HỎA TIỄN, MÁY GIẶT, MÁY KHOAN, MÁY HÀN, ỔN ÁP, BIẾN THẾ, CÁC LOẠI LINH KIỆN ĐIỆN CƠ...</p>
                    <hr>
                    <p class="text-center" style="font-size: 13.5px;">Đ/C: <span>55 Nguyễn Tất Thành, EaTling, Cư Jút, Đắk Nông <br/> Đối diện nhà thờ Phúc Lộc</span></p>
                    <p class="text-center" style="font-size: 13.5px;">Đ/T: 0261.38833 874 - 0815.279 279 - 0905.159 826 - 0812.679 679</p>          
                </div>
                <div class="text-center" style="width: 25%">
                    <h6 style="font-weight: bold;font-size: 18px; padding-top: 45px">HÓA ĐƠN</h6>
                    @if (count($invoices))
                    <p>{{ $invoiceCate[$invoice->type] }}</p>
                    <?php
                        $date = Carbon::parse($invoice->created_at);
                        $string = '0000000000';
                        $string = substr($string, 0, (10 - strlen($invoice->id)));
                    ?>
                    <p>HĐ: {{ $string}}{{$invoice->id}}</p>
                    @endif
                </div>
            </div>
            @if (count($invoices))
                <div class="row">
                    <div class="col-6 text-left">
                        KH: {{ $invoice->customer->name }}
                        <br>
                        SĐT: {{ $invoice->customer->phone }}
                        <br>
                        ĐC: {{ $invoice->customer->address }}
                        <br>
                        {{-- Nhân viên: {{ Auth::user()->full_name }} --}}
                    </div>
                    <div class="col-6 text-right">
                        Ngày: {{ $date->format('d/m/Y') }}
                        <br>
                        In lúc: {{ $date->format('H:i') }}
                    </div>  
                </div>
                <table class="table table-bordered print">
                    <tr class="text-center">
                        <th>Mặt hàng</th>
                        <th>SL</th>
                        <th>ĐVT</th>
                        <th>Giá</th>
                        <th>CK (%)</th>
                        <th>T.tiền</th>
                    </tr>
                    @foreach($invoices as $order)
                        <tr>
                            <td>{{ $order->namePro }}</td>
                            <td class="text-center">{{ $order->quantity }}</td>
                            <td class="text-center">{{ $arrUnit[$order->unitId] }}</td>
                            <td class="text-right">{{ number_format($order->price) }}</td>
                            <td class="text-right">{{ number_format($order->discount) }}</td>
                            <td class="text-right">{{ number_format(Invoice::calculationDiscountInvoice($order->price,$order->quantity,$order->discount)) }}</td>
                        </tr>
                    @endforeach
                </table>
                <div class="row">
                    <div class="col-6 text-left">
                        Tổng:
                        <br>
                        Thanh toán:
                        <br>
                        Hóa đơn nợ:
                        <br>
                        Tổng tiền còn nợ:
                    </div>
                    <div class="col-6 text-right">
                        <b>{{ number_format($invoice->total_money) }}</b>
                        <br>
                        <b>{{ number_format($invoice->guest_pay) }}</b>
                        <br>
                        <b>{{number_format($invoice->guest_pay - $invoice->total_money) }}</b>
                        <br>
                        <b>{{ number_format($invoice->debt) }}</b>
                    </div>  
                </div>
            @endif
            <p><i>Cảm ơn Quý khách hẹn gặp lại !</i></p>
        </center>
    </div>
    <script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>