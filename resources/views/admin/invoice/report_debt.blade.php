@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        #pageReport .list {
            margin-top: 20px;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;
    use App\InvoiceDetail;

    $invoiceDetail = new InvoiceDetail;
    $arrUnit = $invoiceDetail->nameUnit();
    $strUnit = json_encode($arrUnit);
?>

@section('content')
<div id="pageReport">
    <div class="row">
        <h4>Danh sách hóa đơn còn nợ.</h4>
    </div>
    <div class="list">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="text-center table-info">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col" style="width: 80px;">Mã HD</th>
                    <th scope="col">Tên khách hàng</th>
                    <th scope="col">Tên nhân viên</th>
                    <th scope="col">Ngày bán</th>
                    <th scope="col">Loại HĐ</th>
                    <th scope="col">Ghi chú</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if (count($invoices))
                    <?php $i = Core::getSTT($invoices) ?>
                    @foreach($invoices as $invoice)
                        <th scope="row" class="text-center">{{ ++$i }}</th>
                            <td class="text-center">{{ $invoice->id }}</td>
                            <td>{{ $invoice->customer->name }}</td>
                            <td>{{ $invoice->user->full_name }}</td>
                            <td>{{ Carbon::parse($invoice->created_at)->format("d-m-Y H:i") }}</td>
                            <td class="text-center">
                                @if ($invoice->type == Invoice::WHOLESALE)
                                    <span class="badge badge-success">Bán sỉ</span>
                                @else
                                    <span class="badge badge-primary">Bán lẻ</span>
                                @endif
                            </td>
                            <td class="max-with-md-300">{{ $invoice->note }}</td>
                            <td class="">
                                <button type="button" class="btn btn-info modal-view" title="Xem chi tiết" data-toggle="modal" data-target="#myModal"
                                data-invoice="{{ json_encode([$invoice->total_money,$invoice->guest_pay]) }}"
                                data-detail="{{ $invoice->detail }}"><i class="fas fa-eye"></i>
                                </button>
                            </td>
                    @endforeach
                @endif
            </tbody>
        </table>
        <div class="text-center">
            {{ $invoices->links() }}
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title text-center">Thông tin hóa đơn</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 detail">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>stt</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Số lượng</th>
                                        <th>DVT</th>
                                        <th>Giá</th>
                                    </tr>
                                </thead>
                                <tbody class="body-detail">
                                    <tr>
                                        <td>data</td>
                                        <td>data</td>
                                        <td>data</td>
                                        <td>data</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="table-total-money">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Tổng tiền:</td>
                                            <td class="total"><span>0</span> đ</td>
                                        </tr>
                                        <tr>
                                            <td>Thanh toán:</td>
                                            <td class="guest_pay"><span>0</span> đ</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        var strUnit = '<?php echo"$strUnit" ?>';
    </script>
    <script src="{{ asset('js/invoice/modal_invoice_deatail.js')}}"></script>
@endsection
