@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;
    
    $objInvoice = new Invoice();
?>

@section('content')
<div id="list_invoice">
    <form action="{{route('admin.invoice.list')}}" method="get" class="form-inline" >
        <div class="text-right" style="width: 100%; margin-bottom: 10px">
            <a href="{{route('admin.invoice.list')}}" class="btn btn-info" role="button">Refresh tìm kiếm</a>
            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> tìm kiếm</button>
        </div>
        <table class="table table-bordered ">
            <thead>
                <tr class="text-center bg-info">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col" class="width-150">Mã HD</th>
                    <th scope="col">Tên khách hàng</th>
                    {{-- <th scope="col">Tên nhân viên</th> --}}
                    <th scope="col">Ngày bán</th>
                    <th scope="col">Loại HĐ</th>
                    <th scope="col">Ghi chú</th>
                    <th scope="col">Trạng thái</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>
                        <input type="search" name="invoice_id"  class="form-control" placeholder="search mã hóa đơn" style="width: 100%"
                        value="{{ array_key_exists('invoice_id', $dataSearch) ? $dataSearch['invoice_id'] : ''}}" style="width: 80px;">
                    </td>
                    <td>
                        <input type="search" name="customer_name"  class="form-control with-100_"
                        value="{{ array_key_exists('customer_name', $dataSearch) ? $dataSearch['customer_name'] : ''}}" placeholder="search tên khách hàng">
                    </td>
                    <td>
                        <input type="date" name="customer_date"  class="form-control" style="width: 100%"
                        value="{{ array_key_exists('customer_date', $dataSearch) ? $dataSearch['customer_date'] : ''}}"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @if (count($invoices))
                    <?php $i = Core::getSTT($invoices) ?>
                    @foreach($invoices as $invoice)
                        <tr>
                            <th scope="row" class="text-center">{{ ++$i }}</th>
                            <td class="text-center"> {{ $objInvoice->getCodeInvoice($invoice->id) }}</td>
                            <td>{{ $invoice->customer->name }}</td>
                            {{-- <td>{{ $invoice->user->full_name }}</td> --}}
                            <td>{{ Carbon::parse($invoice->created_at)->format("d-m-Y H:i") }}</td>
                            <td class="text-center">
                                @if ($invoice->type == Invoice::WHOLESALE)
                                    <span class="badge badge-pill badge-success">Bán sỉ</span>
                                @else
                                    <span class="badge badge-pill badge-primary">Bán lẻ</span>
                                @endif
                            </td>
                            <td class="max-with-md-300">{{ $invoice->note }}</td>
                            <td class="text-center">
                                <?php $status = $objInvoice->getStatus($invoice); ?>
                                @if ($invoice->status == Invoice::ACTIVE)
                                    <span class="badge badge-success">{{ $status }}</span>
                                @else
                                    <span class="badge badge-danger">{{ $status }}</span>
                                @endif
                            </td>
                            <td class="eye-pen">
                                <button type="button" class="btn btn-info modal-view" title="Xem chi tiết" data-toggle="modal"data-target="#myModal"
                                data-invoice="{{ json_encode([$invoice->total_money,$invoice->guest_pay]) }}"
                                data-detail="{{ $invoice->detail }}"
                                ><i class="fas fa-eye"></i></button>
                                <button type="button" class="btn btn-danger huy-don" title="Hủy hóa đơn" data-toggle="modal" data-target="#myModalDelte" data-id="{{ $invoice->id }}"><i class="fas fa-times" style="padding: 3px 4px"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </form>
    <div class="text-center">
        {{ $invoices->links() }}
    </div>

    @include('admin.invoice.modals.invoice_detail')
    <!-- The Modal -->
    <div class="modal fade" id="myModalDelte">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                      <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title text-center">Báo hủy hóa đơn</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{ route('admin.invoice.status')}}"  method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <input type="text" name="id" hidden>
                            <div class="form-group" style="width: 100%; padding: 0px 15px">
                              <label for="comment">Nhập lý do hủy đơn:</label>
                              <textarea class="form-control" rows="5" id="comment" name="note" required></textarea>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Gửi</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#list_invoice').on('click', "form .huy-don", function (argument) {
            $("#myModalDelte").find('input[name="id"]').val($(this).attr('data-id'));
        })
    </script>
@endsection
