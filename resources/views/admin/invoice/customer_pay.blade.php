@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <style>
        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .search {
            border: 1px solid #ff23002e;
            padding: 15px;
            box-shadow: 0px 1px 6px 0px #e6909059;
        }
        .search .span {
            margin-right: 20px
        }
        #customer-pay .infor-cus {
            font-size: 16px;
            line-height: 2;
        }
        #customer-pay .cus-pay {
            border-left: 1px solid red;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;
    use App\InvoiceDetail;

    $invoiceDetail = new InvoiceDetail;
    $arrUnit = $invoiceDetail->nameUnit();
    $strUnit = json_encode($arrUnit);

    $check = false;
    if ($dataSearch['id_cus'] || $dataSearch['customer_name'] || $dataSearch['customer_phone']) {
        $check = true;
    }
?>

@section('content')
   <div id="customer-pay">
        <p>Thanh toán nợ của khách hàng</p>
        <form action="{{ route('admin.invoice.customer_pay') }}" method="get">
            <div class="row">
                <div class="col-md-5 offset-md-3">
                    <div class="search">
                        <div class="col-md-12">
                            <p class="cate"><span class="span">Loại khách hàng: </span>
                            <input type="checkbox" id="toggle-two" checked name="type" data-offstyle="success" data-size="normal">
                            </p>
                        </div>
                        <div class="col-md-12 ">
                            <div class="search-name">Họ và tên khách hàng:</div>
                            <div class="search-phone d-none">Số điện thoại:</div>
                        </div>
                        <div class="col-md-12">
                            <div class="search-name">
                               <input type="text" name="customer_name" class="form-control" placeholder="Họ và tên" id="cus_name">
                            </div>
                            <div class="search-phone d-none">
                                <input type="number" name="customer_phone" class="form-control" placeholder="Số điện thoại" id="cus_phone">
                            </div>
                        </div>
                        <div class="col-md-12 text-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">Tìm kiếm khách hàng</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="mt-3">
            @if ($check && (($array && !count($customer) || (!$array && !$customer))))
            <p>Không tìm thấy khác hàng</p>
            @endif
        </div>
        <div class="row mt-3">
           <div class="col-12">
                <hr>
                @if ($array && isset($customer) && count($customer))
                    <h5>Có cách khánh hàng sau phù hợp với tìm kiếm trên: </h5>
                    <div class="mt-3">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tên khách hàng</th>
                                    <th>Loại KH</th>
                                    <th>Số điện thoại</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($customer as $item)
                                  <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        @if ($item->type == Invoice::WHOLESALE)
                                            <span class="badge badge-pill badge-success">Bán sỉ</span>
                                        @else
                                            <span class="badge badge-pill badge-primary">Bán lẻ</span>
                                        @endif
                                    </td>
                                    <td>{{ $item->phone }}</td>
                                    <td><a href="{{ route('admin.invoice.customer_pay') }}?id_cus={{$item->id}}&customer_name={{ $item->name }}">Chọn thanh toán</a></td>
                                  </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                @endif

                {{-- thông tin khách hàng --}}
                @if (!$array && isset($customer))
                    <div class="row">
                        <div class="col-md-7">
                            <h5>Thông tin khách hàng</h5>
                            <ul class="infor-cus">
                                <li><b>Tên khách hàng: {{ $customer->name }}</b></li>
                                <li>Số điện thoại: {{ $customer->phone }}</li>
                                <li>Địa chỉ: {{ $customer->address }}</li>
                                <li>Loại khách hàng: 
                                    @if ($customer->type == Invoice::WHOLESALE)
                                        Bán sỉ
                                    @else
                                        Bán lẻ
                                    @endif
                                </li>
                                <li>
                                    @if ($customer->debt > 0)
                                        Tiền dư: {{ number_format($customer->debt)}}
                                    @else
                                        <b>Tiền nợ: {{ number_format($customer->debt)}} đ</b>
                                    @endif
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 cus-pay">
                            <form action="{{route('admin.invoice.customer_pay_post')}}" id="form-cus-pay" method="post" >
                                @csrf
                                <div class="form-group">
                                    <label for="pay">Nhập số tiền thanh toán: </label>
                                    <input type="text" id="pay" name="cus_pay" class="form-control money-pay" data-type="currency" required>
                                  </div>
                                <input type="number" name="cus_id" value="{{ $customer->id }}" hidden>
                                <button type="button" class="btn btn-info form-control btn-pay">Thanh toán</button>
                            </form>
                        </div>
                    </div>
                @endif
                @if (!$array && isset($cusPays))
                    <hr class="mt-3">
                    <h5 class="mt-3">Lịch sử trả tiền nợ của khách hàng:</h5>
                    @if (count($cusPays))
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>NV thanh toán</th>
                                    <th>Số tiền</th>
                                    <th>Ngày thanh toán</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cusPays as $item)
                                  <tr>
                                    <td>{{ $item->nameEmp }}</td>
                                    <td>{{ number_format($item->guest_pay) }}</td>
                                    <td>{{ Carbon::parse($item->created_at)->format('d-m-Y') }}</td>
                                  </tr>
                                @endforeach
                            </tbody>
                          </table>
                    @endif
                @endif
           </div>
        </div>
   </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('#toggle-two').bootstrapToggle({
               on: 'Bán sỉ',
               off: 'Bán lẻ'
            });
        })
    </script>
    <script>
        $(function() {
            $('#toggle-two').change(function() {
                if ($(this).prop('checked')) {
                    $(".search-name").removeClass('d-none');
                    $(".search-phone").removeClass('d-block');
                    $(".search-name").addClass('d-block');
                    $(".search-phone").addClass('d-none');
                } else {
                    $(".search-name").removeClass('d-block');
                    $(".search-phone").removeClass('d-none');
                    $(".search-name").addClass('d-none');
                    $(".search-phone").addClass('d-block');
                }
            })
          })
    </script>
    <script>
        $('form').on('click', '.btn-pay', function() {
            var money = $('.money-pay').val();
            if (money == '') {
                alert('Số tiền thanh toán đang trống')
            } else {
                if (confirm("Bạn có chắc thanh toán số tiền: " + money)) {
                    $("#form-cus-pay").submit();
                }
            }
        })
    </script>
    <script>
        $("input[data-type='currency']").on({
            keyup: function() {
              formatCurrency($(this));
            },
            blur: function() { 
              formatCurrency($(this), "blur");
            }
        });


        function formatNumber(n) {
          // format number 1000000 to 1,234,567
          return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
          // appends $ to value, validates decimal side
          // and puts cursor back in right position.
          
          // get input value
          var input_val = input.val();
          
          // don't validate empty input
          if (input_val === "") { return; }
          
          // original length
          var original_len = input_val.length;

          // initial caret position 
          var caret_pos = input.prop("selectionStart");
            
          // check for decimal
          if (input_val.indexOf(".") >= 0) {

            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // validate right side
            right_side = formatNumber(right_side);
            
            // On blur make sure 2 numbers after decimal
            if (blur === "blur") {
              right_side += "00";
            }
            
            // Limit decimal to only 2 digits
            right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

          } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;
          }
          
          // send updated string to input
          input.val(input_val);

          // put caret back in the right position
          var updated_len = input_val.length;
          caret_pos = updated_len - original_len + caret_pos;
          input[0].setSelectionRange(caret_pos, caret_pos);
        }
    </script>
@endsection
