@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        #pageReport .list {
            margin-top: 20px;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;
    use App\InvoiceDetail;

    $invoiceDetail = new InvoiceDetail;
    $arrUnit = $invoiceDetail->nameUnit();
    $strUnit = json_encode($arrUnit);
?>

@section('content')
<div id="pageReport">
    <div class="row">
        <div class="col-sm-4">
            <table class="table table-bordered">
                <tr class="table-success">
                    <td>Tổng số hóa đơn </td>
                    <td>{{ $invoices->total() }}</td>
                </tr>
                <tr class="table-danger">
                    <td>Tổng tiền hóa đơn</td>
                    <td>{{ number_format($totalMoneyInvoice) }}</td>
                </tr>
                <tr class="table-info">
                    <td>Tổng tiền thanh toán</td>
                    <td>{{ number_format($totalMoney) }}</td>
                </tr>
            </table>
        </div>
        <div class="col-sm-8">
            <form class="form-inline" action="{{ route('admin.invoice.report')}}">
                <div class="form-group ">
                    <label for="startDate" style="width: 100%; justify-content: left;">Từ ngày</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                        </div>
                        <input type="text" class="form-control datetimepicker1" name="startdate" placeholder="start date" value="{{ $date['start'] }}">
                    </div>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="enddate" style="width: 100%; justify-content: left;">Đến ngày</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                        </div>
                        <input type="text" class="form-control datetimepicker1" name="enddate" placeholder="end date" value="{{ $date['end'] }}">
                    </div>
                </div>
              <button type="submit" class="btn btn-primary mb-2">Tìm kiếm</button>
            </form>
            <script type="text/javascript">
                $(function () {
                    $('.datetimepicker1').datepicker({
                        format: 'dd-mm-yyyy',
                    });
                });
            </script>
        </div>
    </div>
    <div class="list">
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr class="text-center table-info">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col" style="width: 80px;">Mã HD</th>
                    <th scope="col">Tên khách hàng</th>
                    <th scope="col">Tên nhân viên</th>
                    <th scope="col">Ngày bán</th>
                    <th scope="col">Tổng tiền</th>
                    <th scope="col">Thanh toán</th>
                    <th scope="col">Ghi chú</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if (count($invoices))
                    <?php $i = Core::getSTT($invoices) ?>
                    @foreach($invoices as $invoice)
                        <tr class="text-center">
                            <th scope="row">{{ ++$i }}</th>
                            <td>{{ $invoice->id }}</td>
                            <td class="text-left">{{ $invoice->customer->name }}</td>
                            <td>{{ $invoice->user->full_name }}</td>
                            <td>{{ Carbon::parse($invoice->created_at)->format("d-m-Y H:i") }}</td>
                            <td>{{ number_format($invoice->total_money) }}</td>
                            <td>{{ number_format($invoice->guest_pay) }}</td>
                            <td class="max-with-md-300">{{ $invoice->note }}</td>
                            <td class="">
                                <button type="button" class="btn btn-info modal-view" title="Xem chi tiết" data-toggle="modal" data-target="#myModal"
                                data-invoice="{{ json_encode([$invoice->total_money,$invoice->guest_pay]) }}"
                                data-detail="{{ $invoice->detail }}"><i class="fas fa-eye"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <div class="text-center">
            {{ $invoices->links() }}
        </div>
    </div>

    <!-- The Modal -->
    @include('admin.invoice.modals.invoice_detail')
</div>
@endsection

@section('script')
    <script>
        var strUnit = '<?php echo"$strUnit" ?>';
    </script>
    <script src="{{ asset('js/invoice/modal_invoice_deatail.js')}}"></script>
@endsection
