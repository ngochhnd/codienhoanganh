<?php
    use App\InvoiceDetail;

    $invoiceDetail = new InvoiceDetail;
    $arrUnit = $invoiceDetail->nameUnit();
    $strUnit = json_encode($arrUnit);
?>
<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title text-center">Thông tin hóa đơn</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 detail">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="with-10">stt</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>DVT</th>
                                    <th>Giá</th>
                                    <th>CK (%)</th>
                                </tr>
                            </thead>
                            <tbody class="body-detail">
                                <tr>
                                    <td>data</td>
                                    <td>data</td>
                                    <td>data</td>
                                    <td>data</td>
                                    <td>data</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="table-total-money">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Tổng tiền:</td>
                                        <td class="total"><span>0</span> đ</td>
                                    </tr>
                                    <tr>
                                        <td>Thanh toán:</td>
                                        <td class="guest_pay"><span>0</span> đ</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    var strUnit = '<?php echo"$strUnit" ?>';
</script>
<script src="{{ asset('js/invoice/modal_invoice_deatail.js')}}"></script>