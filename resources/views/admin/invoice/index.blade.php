@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <style>
        .badge {
            padding: 5px .6em;
        }
    </style>
@endsection

<?php
    use Carbon\Carbon;
    use App\Core;
    use App\Invoice;

    $objInvoice = new Invoice();
?>

@section('content')
<div id="list_invoice">
    <form action="{{route('admin.invoice.index')}}" method="get" class="form-inline" >
        <div class="text-right" style="width: 100%; margin-bottom: 10px">
            <h3 class="text-left">Danh sách hóa đơn</h3>
            <a href="{{route('admin.invoice.index')}}" class="btn btn-info" role="button">Refresh tìm kiếm</a>
            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> tìm kiếm</button>
        </div>
        <table class="table table-bordered ">
            <thead>
                <tr class="text-center bg-info">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col">Mã HD</th>
                    <th scope="col">Tên khách hàng</th>
                    <th scope="col">Tên nhân viên</th>
                    <th scope="col">Ngày bán</th>
                    <th scope="col">Loại HĐ</th>
                    {{-- <th scope="col">Ghi chú</th> --}}
                    <th scope="col">Trạng thái</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td>
                        <input type="search" name="invoice_id"  class="form-control" placeholder="mã hóa đơn" style="width: 100%"
                        value="{{ array_key_exists('invoice_id', $dataSearch) ? $dataSearch['invoice_id'] : ''}}" style="width: 80px;">
                    </td>
                    <td>
                        <input type="search" name="customer_name"  class="form-control" style="width: 100%"
                        value="{{ array_key_exists('customer_name', $dataSearch) ? $dataSearch['customer_name'] : ''}}" placeholder="search tên khách hàng">
                    </td>
                    <td>
                        <select name="user_id" id="user_id"  class="form-control">
                            <option value="">Tất cả nhân viên</option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}" 
                                <?php 
                                    if (array_key_exists('user_id', $dataSearch) &&
                                        $dataSearch['user_id'] == $user->id ) {
                                        echo 'selected';
                                    }
                                ?>
                                >{{ $user->full_name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="date" name="customer_date"  class="form-control" style="width: 100%"
                        value="{{ array_key_exists('customer_date', $dataSearch) ? $dataSearch['customer_date'] : ''}}"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @if (count($invoices))
                    <?php $i = Core::getSTT($invoices) ?>
                    @foreach($invoices as $invoice)
                        <tr>
                            <th scope="row" class="text-center">{{ ++$i }}</th>
                            <td class="text-center width-150"> {{ $objInvoice->getCodeInvoice($invoice->id) }}</td>
                            <td>{{ $invoice->customer->name }}</td>
                            <td>{{ $invoice->user->full_name }}</td>
                            <td class="text-center">{{ Carbon::parse($invoice->created_at)->format("d-m-Y H:i") }}</td>
                            <td class="text-center">
                                @if ($invoice->type == Invoice::WHOLESALE)
                                    <span class="badge badge-pill badge-success">Bán sỉ</span>
                                @else
                                    <span class="badge badge-pill badge-primary">Bán lẻ</span>
                                @endif
                            </td>
                            {{-- <td class="max-with-md-300">{{ $invoice->note }}</td> --}}
                            <td class="text-center">
                                <?php $status = $objInvoice->getStatus($invoice); ?>
                                @if ($invoice->status == Invoice::ACTIVE)
                                    <span class="badge badge-success">{{ $status }}</span>
                                @else
                                    <span class="badge badge-danger">{{ $status }}</span>
                                @endif
                            </td>
                            <td class="eye-pen">
                                <button type="button" class="btn btn-info modal-view" title="Xem chi tiết" data-toggle="modal"data-target="#myModal"
                                data-invoice="{{ json_encode([$invoice->total_money,$invoice->guest_pay]) }}"
                                data-detail="{{ $invoice->detail }}"
                                ><i class="fas fa-eye"></i></button>
                                <button type="button" class="btn btn-secondary print-detail btnprn" title="In hóa đơn" data-id="{{ $invoice->id }}">
                                    <i class="fas fa-print"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </form>
    <div class="text-center">
        {{ $invoices->links() }}
    </div>

    @include('admin.invoice.modals.invoice_detail')

</div>
@endsection

@section('script')
    <script src="{{ asset('js/print/print_order.js')}}"></script>
    <script>
        var urlPrint = "{{ route('invoice/print', ['id' => 'tamp']) }}";
        $('#list_invoice').on('click', "form .huy-don", function (argument) {
            $("#myModalDelte").find('input[name="id"]').val($(this).attr('data-id'));
        })

        $("form").on('click', '.print-detail', function() {
            urlPrint = urlPrint.replace('tamp', $(this).attr('data-id'));
            $('.btnprn').printPage('<a href="' + urlPrint + '" class="btnprn">Print Preview</a>');
        });
    </script>
@endsection
