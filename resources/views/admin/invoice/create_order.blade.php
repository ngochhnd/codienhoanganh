@extends('admin.layouts.default')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/invoice/invoice.css') }}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection

@section('content')
<div class="alert alert-danger print-error-msg" style="display:none">
    <ul style="margin: 0px"></ul>
</div>
<div id="invoice" class="create_invoice">
    <div class="invo-header text-center">
        {{-- <h3>Tạo hóa đơn bán hàng</h3> --}}
    </div>
    <form action="" method="get"  id="form-create-order">
        @csrf
        <div class="invo-header-cus">
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label text-right-sm">Họ và tên:<sup class="red">*</sup></label>
                <div class="col-sm-5">
                    {{-- <input type="name" name="customer_name" class="form-control" placeholder="Nhập tên khách hàng" required> --}}
                    <select  id="customerName" name="customer_name" class="form-control" placeholder="Nhập tên khách hàng"></select>
                </div>
                <div class="col-sm-2">
                    <input type="checkbox" id="toggle-two" name="type" data-offstyle="success">
                    <script>
                      $(function() {
                        $('#toggle-two').bootstrapToggle({
                          on: 'Bán sỉ',
                          off: 'Bán lẻ'
                        });
                      })
                    </script>
                </div>
                <div class="col-sm-3 text-right-sm">
                    <button type="reset" class="btn btn-success reset-table-order">Refresh hóa đơn</button>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label text-right-sm">Số điện thoại:</label>
                <div class="col-sm-5">
                    <input type="number" name="customer_phone" class="form-control" placeholder="Nhập số điện thoại" id="cus_phone">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label text-right-sm">Địa chỉ:</label>
                <div class="col-sm-5">
                    <input type="address" name="customer_address" class="form-control" placeholder="Nhập địa chỉ" id="cus_address">
                </div>
                {{-- <div class="col-sm-5 text-right-sm">
                    <button type="submit" class="btn btn-primary btnprn" id="save-order">Lưu hóa đơn</button>
                </div> --}}
            </div>
            
        </div>
        <table class="table table-bordered table-hover table-order-create">
            <thead>
                <tr class="text-center bg-color-blue">
                    <th scope="col" class="with-10">#</th>
                    <th scope="col" class="width-min-250">Tên hàng</th>
                    <th scope="col" class="width-c_order-100">Đơn vị</th>
                    <th scope="col" class="width-c_order-150">Số lượng</th>
                    <th scope="col" class="width-c_order-200">Đơn giá</th>
                    <th scope="col" class="width-c_order-200">CK (%)</th>
                    <th scope="col" class="width-c_order-200">Thành tiền</th>
                </tr>
            </thead>
            <tbody>
                <?php $hidden = '' ?>
                @for ($i = 0; $i < 20; $i++)
                {{-- <td><input type="text" name='name[]' class="text-left"></td> --}}
                <?php if($hidden == '' && $i > 4) {
                        $hidden = 'd-none';
                    }
                ?>
                <tr class="{{ $hidden }} row-{{ $i }}">
                    <th scope="row">{{ ($i + 1 )}}</th>
                    <td>
                        <select name="name[]" class="form-control search-name-production-invoice"></select>
                    </td>
                    <td>
                        <select class="form-control form-control-sm" name='unit[]'>
                            @foreach($arrUnit as $key => $unit)
                                <option value="{{ $key }}">{{ $unit }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td><input type="number" name='quantity[]' class="type-quantity input-quantity" min="0"></td>
                    <td><input type="text" name='price[]' data-type="currency" class="type-quantity input-price" min="0"></td>
                    <td><input type="text" name="discount[]" class='allownumericwithdecimal type-quantity input-discount'></td>
                    <td class="into-money"></td>
                </tr>
                @endfor
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-6">
                <button type="button" class="btn btn-info text-left" id="add-new-order" data-row="5">Thêm 5 dòng</button>
                <button type="button" class="btn btn-primary btnprn" style="margin-left: 40px" id="save-order">Lưu và in hóa đơn</button>
            </div>
            <div class="col-md-6 box-money">
                <table>
                    <tbody>
                        <tr>
                            <td>Tổng tiền hóa đơn: </td>
                            <td class="total-money text-right"><span>0 &#8363;</span></td>
                        </tr>
                        <tr>
                            <td class="invoice-debt">Hóa đơn nợ: </td>
                            <td class="text-right money_debt"><span>0</span> đ</td>
                        </tr>
                        <tr>
                            <td>Thanh toán: </td>
                            <td class="text-right"><input type="text" name="guests_pay" id="currency-field" value="0" data-type="currency" > ₫</div></td>
                        </tr>
                        <tr>
                            <td class="text-debt">Còn nợ</td>
                            <td class="text-right money_total_debt"><span>0 đ</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
    <script>
        var  urlPrint = "{{ route('invoice/print', ['id' => 'tamp']) }}";
        var  urlInvoiceSave = "{{ route('invoice/save') }}";
        var urlSerachCustomer = "{{ route("admin.search.customer_invoice") }}";
        var urlCustomerInfor = "{{ route('admin.custome_invoice_infor')}}";
        var urlSearchNameProInvoice = "{{ route('admin.invoice.search.name_production')}}";
    </script>

    <script src="{{ asset('js/invoice/create.js')}}"></script>
    <script src="{{ asset('js/print/print_order.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">
     
    </script>
@endsection