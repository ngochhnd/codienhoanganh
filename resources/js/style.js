$('.delete-message-name').on('click', function() {
    if (confirm($(this).attr('data-message') + $(this).attr('data-name'))) {
        $(this).find('form').submit();
    }
})

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}