$('.btn-modal-permission').on('click', function() {
    var roles = $('.list-roles').find('li.active').attr('data-roles-id');
    var nameRoles = $('.list-roles').find('li.active').text();
    var arrPer = [];
    var html = '';
    $("input[type=checkbox]").each(function(index) {
        if (this.checked) {
            arrPer.push($(this).val());
            html = html + '<li>' + $(this).next().text() + '</li>';
        }
    });

    $(".modal-body").find('.md-list-per').html('');
    $(".modal-body").find('.name-role').text(nameRoles);
    $(".modal-body").find('#id-role').val(roles);
    $(".modal-body").find('.md-list-per').append(html);
    $(".modal-body").find('#arr-per').val(arrPer);
})

$('.list-roles').on('click', 'li', function() {
    $('.list-roles li').each(function() {
        $(this).removeClass('active');
    })
    var per = '';
    if ($(this).attr('data-pre') !== '') {
        per = jQuery.parseJSON($(this).attr('data-pre'));
    }
    $("input[type=checkbox]").each(function(index) {
        $(this).prop('checked', false);
        if (per.indexOf($(this).attr('data-name')) > -1) {
            $(this).prop('checked', true);
        }
    });

    $(this).addClass('active');
});