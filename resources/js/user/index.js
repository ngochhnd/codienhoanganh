$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

$('table.view-user').on("click", ".modal-view", function() {
    var data = JSON.parse($(this).attr("data-row"));
    $('.modal-col1').html('');
    $('.modal-col2').html('');

    var birthday = new Date(data.created_at);
    var col1 = '<p><b>Họ và tên: </b>' + data.full_name + '</p>' +
        '<p><b>Tên đăng nhập: </b>' + data.name + '</p>' +
        '<p><b>Ngày sinh: </b>' + data.birthday + '</p>' +
        '<p><b>Ngày tạo: </b>' + birthday.getDate() + "-" + (birthday.getMonth() + 1) + "-" + birthday.getFullYear() + '</p>';
    col1 = col1.replace(/null/g, '');
    $('.modal-col1').append(col1);

    var col2 = '<p><b>Số Điện thoại: </b>' + data.phone + '</p>' +
        '<p><b>Email: </b>' + data.email + '</p>' +
        '<p><b>Địa chỉ: </b>' + data.address + '</p>';
    col2 = col2.replace(/null/g, '');
    $('.modal-col2').append(col2);
})
