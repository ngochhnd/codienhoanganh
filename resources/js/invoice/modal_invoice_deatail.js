$('.modal-view').on('click', function() {
    var units = JSON.parse(strUnit);
    var detail = JSON.parse($(this).attr('data-detail'));
    var invoice = JSON.parse($(this).attr('data-invoice'));
    var n = detail.length;
    $('.detail .body-detail').html('');
    var html = '';
    for (var i = 0; i < n; i++) {
        html += '<tr>'
            + '<td>' + (i + 1) + '</td>'
            + '<td>' + detail[i].product_name + '</td>'
            + '<td>' + detail[i].quantity + '</td>'
            + '<td>' + units[detail[i].unit_id] + '</td>'
            + '<td>' + formatNumber(detail[i].price) + '</td>'
            + '<td>' + detail[i].discount + '</td>'
            + '</tr>';
    }
    $('.detail .body-detail').append(html);
    $('.detail .total span').text(formatNumber(invoice[0]));
    $('.detail .guest_pay span').text(formatNumber(invoice[1]));    
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}