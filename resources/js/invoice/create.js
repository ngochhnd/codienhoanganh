$('.reset-table-order').on('click', function() {
    resetForm();
});

$("#add-new-order").on('click', function() {
    var that = this;
    var maxRow = parseInt($(this).attr('data-row'));
    var i = 0;
    $('.table-order-create tbody tr').each(function() {
        var tamp = maxRow + i;
        if($(this).attr("class").includes('row-' + tamp) && i < 5) {
            $(this).removeClass('d-none');
            i++;
        }
        if ( i > 5) {
            return;
        }
    });
    $(that).attr('data-row', maxRow + 5);
    if ($(that).attr('data-row') == 20) {
        $(that).addClass('d-none');
    }
    // alert($(this).attr('data-row'));
    // var maxRow = $('table.table-order-create tbody tr:last').find("[scope='row']").text();
    // var select = $('table.table-order-create tbody').find("select")[0].outerHTML;
    // var html = '<tr>' +
    //     '<th scope="row">' + (parseInt(maxRow) + 1) + '</th>' +
    //     '<td><select name="name[]" class="form-control search-name-production-invoice"></select></td>' +
    //     '<td>' + select + '</td>' +
    //     '<td><input type="number" name="number[]" class="type-number input-number" min="0"></td>' +
    //     '<td><input type="text" name="price[]" data-type="currency" class="type-number input-price" min="0"></td>' +
    //     '<td class="into-money"></td>' +
    //     '</tr>';
    // $('table.table-order-create tbody').append(html);
    // searchCustomer();
});

// save and print
$("#form-create-order").on("click", "#save-order", function(event) {
    event.preventDefault();

    var fields = $("#form-create-order").serializeArray(),
        arrName = [],
        arrUnit = [],
        arrNumber = [],
        arrPrice = [];
    $('select[name^="name"]').each(function() {
        if ($(this).val() !== null && ($(this).val() !== '')) {
            arrName.push($(this).val());
        }
    });
    $('select[name^="unit"]').each(function() {
        if ($(this).val() !== "") {
            arrUnit.push($(this).val());
        }
    });
    $('input[name^="quantity"]').each(function() {
        if ($(this).val() !== "") {
            arrNumber.push($(this).val());
        }
    });
    $('input[name^="price"]').each(function() {
        if ($(this).val() !== "") {
            arrPrice.push($(this).val());
        }
    });
    if (arrName.length == 0) {
        alert("Hóa đơn không có sản phẩm");
        return false;
    }
    for (var i = 0; i < arrName.length; i++) {
        if (arrNumber[i] === undefined || arrPrice[i] === undefined) {
            alert("Sản phẩm " + (i + 1) + " chưa có số lượng hoặc giá sản phẩm");
            return false;
            break;
        }
    }
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: urlInvoiceSave,
        type: 'POST',
        dataType: 'json',
        data: {
            data: $('#form-create-order').serializeArray(),
        },
        success: function(result) {
            if (result.succes == 1) {
                var cpUrlPring = urlPrint;
                cpUrlPring = cpUrlPring.replace('tamp', result.data);
                $('.btnprn').printPage('<a href="' + cpUrlPring + '" class="btnprn">Print Preview</a>');
                resetForm();
            } else {
                printErrorMsg(result.error);
            }
        },
        error: function(error) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $(".print-error-msg").find("ul").append('<li> Lỗi hệ thống</li>');
        }
    });
})

// ==== quantity ====

$('table').on('keyup', '.type-quantity', function() {
    var productRow = $(this).closest('tr');
    updateQuantity(productRow);
});

$('table').on('change', '.type-quantity', function() {
    var productRow = $(this).closest('tr');
    updateQuantity(productRow);
});

//calculation into money
function updateQuantity(productRow) {
    var number = parseFloat(productRow.find('.input-quantity').val());
    var price = resetMoney(productRow.find('.input-price').val());
    var money = number * price;
    var discount = parseFloat(productRow.find('.input-discount').val());

    if (Number.isInteger(money)) {
        if (isNaN(discount)) {
            discount = 0;
        }
        money = money * ((100 - discount) / 100);
        productRow.find('.into-money').text(formatMoney(money));
        TotalMoney();
    }
    if (isNaN(number) || isNaN(price) || price < 0 || number < 0) {
        productRow.find('.into-money').text('');
        TotalMoney;
    }
}
// tính tổng tiền nợ
function moneyTotal() {
    var tt = $("#currency-field").val().replace(/\./g, '');
    var moneyDebt = resetMoney($(".money_debt span").text());
    var total  = resetMoneyD($('.total-money').find('span').text());
    if (tt == '') {
        $(".money_total_debt span").text(formatMoney(0 - total + moneyDebt)); 
    } else {
        var debt = parseFloat(tt) - (moneyDebt + total);
        if (debt > -500 && debt < 500 ) {
            $(".text-debt").text('Tiền nợ:');
            $(".money_total_debt span").text(formatMoney(0));
        } else {
            if (debt < 0) {
                $(".text-debt").text('Tiền nợ:');
            } else {
                $(".text-debt").text('Tiền dư:');
            }
            $(".money_total_debt span").text(formatMoney(debt));
        }
    }
}

// thanh toán trừ tiền nợ
$('table').on('keyup', "#currency-field", function (argument) {
    moneyTotal();
})

// Calculation total money
function TotalMoney() {
    var total = 0;
    $('.table-order-create tbody tr').each(function() {
        var strMoney = $(this).find('.into-money').text();
        if (strMoney != "") {
            money = resetMoneyD(strMoney);
            total = total + money;
        }
    });
    $('.total-money').find('span').text(formatMoney(total));
    var moneyDebt = resetMoney($(".money_debt span").text());
    var tt = $("#currency-field").val().replace(/\./g, '');
    var totalDebt = parseFloat(tt) - parseFloat(total) + moneyDebt;
    $(".money_total_debt span").text(formatMoney(totalDebt));
    if (totalDebt < 0) {
        $(".text-debt").text('Tiền nợ:');
    } else {
        $(".text-debt").text('Tiền dư:');
    }
}

function formatMoney(total) {
    return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(total);
}

//===== format price khach tra tien ====
$('#invoice').on('keyup', "input[data-type='currency']", function() {
    formatCurrency($(this));
})


function formatNumber(n) {
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

function formatCurrency(input, blur) {
  var input_val = input.val();
  if (input_val === "") { return; }
  var original_len = input_val.length;
  input_val = formatNumber(input_val);
  input.val(input_val);
  //https://codepen.io/559wade/pen/LRzEjj
}

function printErrorMsg (msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','block');
    $.each( msg, function( key, value ) {
        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
    });
}

// reset money tiền có đ
function resetMoneyD(money)
{
    money = money.substring(0, money.length - 2);
    if (money == '') {
        return 0;
    }
    return parseFloat(money.replace(/\./g, ''));
}
// reset money tiền không có đ
function resetMoney(money)
{
    if (money == '') {
        return 0;
    }
    return parseFloat(money.replace(/\./g, ''));
}

$(document).ready(function() {
    searchCustomer();
    $('#customerName').on('change', function() {
        serachCustomerInfor($(this).val());
    });

});
$('form').on('blur', "#cus_phone", function() {
    serachCustomerInfor($("#customerName").val());
})
$('form').on('change', "#toggle-two", function() {
    $("#customerName").text('');
    $("#cus_phone").val('');
    $("#cus_address").val('');
    serachCustomerInfor($("#customerName").val());
})

function searchCustomer() {
    $("#customerName").select2({
        placeholder: "Nhập tên khách hàng",
        allowClear: true,
        minimumInputLength: 2,
        language: {
            inputTooShort: function () {
              return "Nhập 2 ký tự để tìm kiếm";
            }
        },
        tags:true,
        ajax: {
            url: urlSerachCustomer,
            // dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: $("#toggle-two").prop('checked'),
                }
                return query;
            }
        },
    });
}

function serachCustomerInfor(valueName) {
    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: urlCustomerInfor,
        headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{
            valueName: valueName,
            type:$("#toggle-two").prop('checked'),
            phone: $("#cus_phone").val(),
        },
        success: function (data) {
            if (data.total_debt < 0) {
                $(".invoice-debt").text('Hóa đơn nợ:');
            } else {
                $(".invoice-debt").text('Hóa đơn dư:');
            }
            $('.money_debt').find('span').text(formatNumber('"' + data.total_debt  + '"'));
            if (data.name != '') {
                var check = true;
                $("#customerName > option").each(function() {
                    if (this.text == data.name) {
                        check = false;
                    } else {
                        $(this).remove();
                    }
                });
                if (check) {
                    var newOption = new Option(data.name, data.id, true, true);
                    $('#customerName').append(newOption).trigger('change');
                }
            $('#cus_phone').val(data.phone);
            $('#cus_address').val(data.address);
            }
            moneyTotal();
        },
        error: function (data) {
            alert('Lỗi hệ thống. Vui lòng thử lại sau!');
            location.reload();
        }
    });
}

function resetForm() {
    $("#form-create-order")[0].reset();
    $('.into-money').text('');
    $('.total-money').find('span').text(formatMoney(0));
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display','none');
    $(".text-debt").text('Tiền nợ:');
    $(".money_total_debt span").text(formatMoney(0));
    $(".money_debt span").text(0);
    $("#customerName").empty().trigger('change');
    $('#toggle-two').bootstrapToggle('off');
    $(".search-name-production-invoice").empty().trigger('change');

    var i = 5;
    $('.table-order-create tbody tr').each(function() {
        if($(this).attr("class").includes('row-' + i) && i <= 20) {
            $(this).addClass('d-none');
            i++;
        }
    });
    $('#add-new-order').attr('data-row', 5);
}

$(document).ready(function() {
    $(".search-name-production-invoice").select2({
        placeholder: "",
        allowClear: true,
        minimumInputLength: 2,
        language: {
            inputTooShort: function () {
              return "Nhập 2 ký tự để tìm kiếm";
            }
        },
        tags:true,
        ajax: {
            url: urlSearchNameProInvoice,
            dataType: 'json',
        },
    });
})

$(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});