<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['middleware' => 'localization', 'prefix' => Session::get('app.locale')], function() {

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'qlch',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => ['auth']
], function() {
    Route::get('/', 'HomeController@index')->name('index');

    Route::get('/forgot-password', function () {
        return view('admin.forms.forgot_password');
    })->name('forgot-password');

    Route::get('/user/index','UserController@index')->name('user.index');
    Route::get('/user/create','UserController@create')->name('user.create');
    Route::get('/user/change-password','UserController@changePassWord')->name('user.change_password');
    Route::post('/user/save-change-password','UserController@saveChangePassWord')->name('user.save_change_pass');
    Route::post('/user/register','RegisterController@register')->name('user.register');
    Route::get('/user/edit/{id}','UserController@edit')->name('user.edit');
    Route::post('/user/update/{id}','UserController@update')->name('user.update');
    Route::delete('/user/delete/{id}','UserController@delete')->name('user.delete');

    Route::get('/permission','PermissionController@index')->name('permission.index');
    Route::post('/permission/update','PermissionController@update')->name('permission.update');

    Route::get('/roles','RoleController@index')->name('role.index');
    Route::get('/roles/create','RoleController@create')->name('role.create');
    Route::get('/roles/create/user','RoleController@createUser')->name('role.create_user');
    Route::post('/roles/store/user','RoleController@storeUser')->name('role.store.user');
    Route::post('/roles/save','RoleController@save')->name('role.save');
    Route::post('/roles/update','RoleController@update')->name('role.update');
    Route::delete('/roles/delete/{id}','RoleController@delete')->name('role.delete');

    Route::get('/invoice/index','InvoiceController@index')->name('invoice.index');
    Route::get('/invoice/list','InvoiceController@list')->name('invoice.list');
    Route::get('/invoice/customer-pay','InvoiceController@customerPay')->name('invoice.customer_pay');
    Route::post('/invoice/customer-pay-post','InvoiceController@customerPayPost')->name('invoice.customer_pay_post');
    Route::post('/invoice/change-status','InvoiceController@chaneStatusInvoice')->name('invoice.status');
    Route::get('/invoice/censorship','InvoiceController@getInvoiceCensorship')->name('invoice.censorship');
    Route::post('/invoice/is-censorship','InvoiceController@isCensorship')->name('invoice.is-censorship');
    Route::delete('/invoice/delete/{id}','InvoiceController@delete')->name('invoice.delete');
    Route::get('/invoice/report','InvoiceController@reportInvoice')->name('invoice.report');
    Route::get('/invoice/report/debt','InvoiceController@reportInvoiceDebt')->name('invoice.report.debt');
    Route::get('/invoice/search/name-production','InvoiceController@searchNameProduction')->name('invoice.search.name_production');

    Route::get('/customers','CustomerController@index')->name('customer.index');
    Route::get('/customers/create','CustomerController@create')->name('customer.create');
    Route::get('/customer-edit/{id}','CustomerController@edit')->name('customer.edit');
    Route::get('/customer-debt','CustomerController@getCustomerDebt')->name('customer.debt');
    Route::get('/customer/search/customer-invoice','CustomerController@getCustomerInvoice')->name('search.customer_invoice');
    Route::get('/customer/report/retail','CustomerController@reportRetail')->name('customer.report.retail');
    Route::get('/customer/report/wholesale','CustomerController@reportWholesale')->name('customer.report.wholesale');
    Route::get('/customer/{id}/list-invoice','CustomerController@getlistInvoice')->name('custome_list_invoice');
    Route::post('/customer/invoice-infor','CustomerController@getCustomerInvoiceInfor')->name('custome_invoice_infor');
    Route::post('/customer/store','CustomerController@store')->name('customer.store');
    Route::post('/customer/update/{id}','CustomerController@update')->name('customer.update');
    Route::delete('/customer/delete/{id}','CustomerController@delete')->name('customer.delete');
    Route::get('/customer/history-pays/{id}','CustomerController@historyPays')->name('customer.history_pays');
});

// ==== inoice ====
Route::group([
    'namespace' => 'Admin',
    'middleware' => ['auth']
], function() {
    Route::get('/create-order','InvoiceController@create')->name('create-order');
    Route::post('/invoice/save','InvoiceController@save')->name('invoice/save');
    Route::get('/invoice/print/{id}','InvoiceController@printInvoice')->name('invoice/print');
    Route::get('/invoice/print_back/{id}','InvoiceController@printInvoiceBack')->name('invoice/printBack');
    Route::get('/invoice/printA4/{id}','InvoiceController@printInvoiceA4')->name('invoice/printA4');
});
